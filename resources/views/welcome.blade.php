<!DOCTYPE html>
<html>
<head>
    <title>Passport API Script</title>

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Lato';
            color: #000;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <table>
            <tr>
                <td>
                    Welcome in API Script
                </td>
            </tr>
        </table>
    </div>
</div>
<script>

</script>
</body>
</html>

<?php
/**
 * Created by Zaman.
 * User: root
 * Date: 2/24/19
 * Time: 12:45 PM
 */

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ApiToken extends Model
{
    protected $table = 'api_token';
    protected $guarded=[];
}
<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class CountryInfo extends Model
{

    protected $table = 'country_info';
    protected $guarded=[];
}

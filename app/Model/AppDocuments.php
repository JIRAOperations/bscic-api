<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AppDocuments extends Model {

    protected $table = 'app_documents';
    protected $fillable = [
        'id',
        'process_type_id',
        'ref_id',
        'doc_info_id',
        'doc_name',
        'doc_file_path',
        'is_old_file',
        'is_archive',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    ];

    /*     * *****************************End of Model Function********************************* */
}

<?php

namespace App\Model\DCCI;

use Illuminate\Database\Eloquent\Model;

class DCCIPaymentConfirm extends Model {

    protected $table = 'dcci_cos_payment_confirm';
    protected $guarded = [];
}

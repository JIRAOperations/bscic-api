<?php

namespace App\Model\DCCI;

use Illuminate\Database\Eloquent\Model;

class DCCICosStatusQueue extends Model {

    protected $table = 'dcci_cos_application_status';
    protected $guarded = [];

}

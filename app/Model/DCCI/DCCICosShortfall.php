<?php

namespace App\Model\DCCI;

use Illuminate\Database\Eloquent\Model;

class DCCICosShortfall extends Model {

    protected $table = 'dcci_cos_shortfall';
    protected $guarded = [];

}

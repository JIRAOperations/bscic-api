<?php

namespace App\Model\DCCI;

use Illuminate\Database\Eloquent\Model;

class RequestQueueDCCICos extends Model {

    protected $table = 'dcci_cos_api_request_queue';
    protected $fillable = ['response_json','status'];

}

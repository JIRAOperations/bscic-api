<?php

namespace App\Model\DCCI;

use Illuminate\Database\Eloquent\Model;

class DCCICosApps extends Model {

    protected $table = 'dcci_cos_apps';
    protected $fillable = ['response_json','status'];

}

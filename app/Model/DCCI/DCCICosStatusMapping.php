<?php

namespace App\Model\DCCI;

use Illuminate\Database\Eloquent\Model;

class DCCICosStatusMapping extends Model {

    protected $table = 'dcci_cos_status_mapping';
    protected $guarded = [];

}

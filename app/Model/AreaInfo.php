<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AreaInfo extends Model
{

    protected $table = 'area_info';
    protected $guarded=[];
}
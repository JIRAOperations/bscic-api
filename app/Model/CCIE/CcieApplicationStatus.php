<?php

namespace App\Model\CCIE;


use Illuminate\Database\Eloquent\Model;

class CcieApplicationStatus extends Model {

    protected $table = 'ccie_application_status';
    protected $guarded=[];

}

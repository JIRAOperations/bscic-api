<?php

namespace App\Model\CCIE;

use Illuminate\Database\Eloquent\Model;

class CcieChallanConfirm extends Model {
    protected $table = 'ccie_challan_confirm';
    protected $fillable = array(
        'id',
        'ref_id',
        'tracking_no',
        'request_spg',
        'response_spg',
        'request_ccie',
        'request_ccie',
        'response_ccie',
    );
    protected $guarded=[];



}

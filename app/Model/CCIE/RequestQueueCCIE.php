<?php


namespace App\Model\CCIE;

use Illuminate\Database\Eloquent\Model;

class RequestQueueCCIE extends Model
{

    protected $table = 'ccie_api_request_queue';
    protected $guarded = [];

}

<?php

namespace App\Model\CCIE;

use Illuminate\Database\Eloquent\Model;

class CcieStatusMapping extends Model {

    protected $table = 'ccie_status_mapping';
    protected $guarded=[];

}

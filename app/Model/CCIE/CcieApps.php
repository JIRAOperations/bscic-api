<?php

namespace App\Model\CCIE;

use Illuminate\Database\Eloquent\Model;

class CcieApps extends Model {

    protected $table = 'cci_apps';
    protected $fillable = ['is_submit_shortfall'];

}

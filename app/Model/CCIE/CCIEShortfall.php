<?php
namespace App\Model\CCIE;

use Illuminate\Database\Eloquent\Model;

class CCIEShortfall extends Model {
protected $table = 'ccie_shortfall';
protected $fillable =['ref_id','ccie_tracking_no','submission_response','submission_status'];
protected $guarded = [];

}


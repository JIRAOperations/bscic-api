<?php

namespace App\Model\DOE;

use Illuminate\Database\Eloquent\Model;

class DoeApplicationShortfall extends Model {

    protected $table = 'doe_application_shortfall';
    protected $guarded=[];

}

<?php

namespace App\Model\DOE;

use Illuminate\Database\Eloquent\Model;

class DoeApiRequest extends Model {

    protected $table = 'doe_api_request';
    protected $guarded=[];

}

<?php

namespace App\Model\DOE;

use Illuminate\Database\Eloquent\Model;

class DoeStatusMapping extends Model {

    protected $table = 'doe_status_mapping';
    protected $guarded=[];

}

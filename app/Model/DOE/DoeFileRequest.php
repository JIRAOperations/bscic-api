<?php

namespace App\Model\DOE;

use Illuminate\Database\Eloquent\Model;

class DoeFileRequest extends Model {

    protected $table = 'doe_file_request';
    protected $guarded=[];

}

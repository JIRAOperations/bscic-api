<?php

namespace App\Model\DOE;

use Illuminate\Database\Eloquent\Model;

class DoeMaster extends Model {

    protected $table = 'doe_master';
    protected $guarded=[];

}

<?php

namespace App\Model\DOE;

use Illuminate\Database\Eloquent\Model;

class DoeApplicationStatus extends Model {

    protected $table = 'doe_application_status';
    protected $guarded=[];

}

<?php
/**
 * Created by Zaman.
 * User: root
 * Date: 1/28/19
 * Time: 12:05 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class ProcessList extends Model
{
    protected $table = 'process_list';
    protected $guarded=[];
}
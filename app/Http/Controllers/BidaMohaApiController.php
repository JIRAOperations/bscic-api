<?php

namespace App\Http\Controllers;

use App\Model\MOHA\MohaApiQueue;
use App\ProcessList;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use DB;


class BidaMohaApiController extends Controller
{
    public function applicationStatusUpdate(Request $request){
        try{
            $jwt = $request->token;
            $decoded = JWT::decode($jwt, 'thisissecret', array('HS256'));
            $decoded_json = $request->result;
            if($decoded_json != null){
                $getRefId = ProcessList::where('tracking_no', $decoded_json['app_tracking_id'])->value('ref_id');

                if($getRefId !=  ''){
                    $queue_data = MohaApiQueue::where('ref_id', $getRefId)->first();
                    if($queue_data){
                        $queue_data->status_check_response = json_encode($decoded_json);
                        $queue_data->moha_tracking_id = $decoded_json['moha_tracking_id'];
                        $queue_data->certificate = $decoded_json['certificate'];


                        $queue_data->fl_certificate = $decoded_json['fl_certificate'];
                        $status_id = DB::select(DB::raw('select id from security_clearance_status where process_type_id=2 and mh_status_id = '.$decoded_json['status_id']));

                        if(count($status_id) > 0){
                            $queue_data->status =  $status_id[0]->id;
                        }else{
                            return response()->json(['code' => -1, 'message' => 'Status was not found!']);
                        }
                        $queue_data->save();
                        return response()->json(['code' => 1, 'message' =>'Status Updated Successfully!']);
                    }else{
                        return response()->json(['code' => -1, 'message' => 'Application not found with this tracking ID!']);
                    }
                }else{
                    return response()->json(['code' => -1, 'message' => 'Application not found with this tracking ID!']);
                }

            }else{
                return response()->json(['code' => -1, 'message' => 'Invalid Data']);
            }
        } catch(\Exception $e){
            return response()->json(['code' => $e->getCode(), 'message' => $e->getMessage().$e->getLine()]);
        }
    }
}
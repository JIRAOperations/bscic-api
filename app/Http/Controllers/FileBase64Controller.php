<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Application;


class FileBase64Controller extends Controller
{
    public function getBase64FromUrl(Request $request)
    {


        if ($request->get('fileurl')) {
            $url = $request->get('fileurl');
            $modifyUrl = parse_url($url);
            if (isset($modifyUrl['host'])) {
                $headers = get_headers($url);
                $result = stripos($headers[0], "200 OK") ? true : false;
                if ($result) {
                    $data = $this->base64Generate($url);
                } else {
                    $data = null;
                }
            } else {
                $file_base_path = config('constant.file_root_path');
                if (file_exists($file_base_path . $url)) {
                    $data = $this->base64Generate($file_base_path . $url);
                } else {
                    $data = null;
                }
            }

        } else {
            $data = null;
        }

        return  $data;
    }

    protected function base64Generate($url)
    {
        $type = pathinfo($url, PATHINFO_EXTENSION);
        $file = file_get_contents($url);
        return base64_encode($file);
    }

}
<?php

namespace App\Http\Controllers;

use App\Model\Rjsc\RjscRegistration;
use App\Model\Rjsc\RjscToken;
use Illuminate\Http\Request;
use Laravel\Lumen\Application;


class RjscController extends Controller
{
    public function apiRequest()
    {
        $returnArray = array();
        $paramValue = str_replace('\"', '"', $_REQUEST['param']);
        $returnArray = $this->getParamValue($paramValue);
        $requestType = $returnArray['rjsc']['requestType'];

        switch ($requestType){
            case 'DIVISION':
                $this->divisionList($returnArray['rjsc']);
                break;
            case 'DISTRICT':
                $this->districtList($returnArray['rjsc']);
                break;
            case 'RJSC_OFFICE_LIST':
                $this->rjscOfficeList($returnArray['rjsc']);
                break;
            case 'COMPANY_TYPE':
                $this->companyTypeList($returnArray['rjsc']);
                break;
            case 'POSITION_BY_COM_TYPE':
                $this->positionList($returnArray['rjsc']);
                break;

        }
    }

    function getParamValue($getParam)
    {
        $returnArray = array();
        $returnArray = json_decode($getParam, true);
        #$this->writeLog("Request", $getParam);
        return $returnArray;
    }

    function writeLog($type, $log)
    {
        $fileName = storage_path().'/logs/'. date("Ymd") . ".txt";
        //echo $fileName;die();
        $file = fopen($fileName, "a");
        if ($type == "Request") {
            fwrite($file, "\r### " . date("H:i:s") . "\t" . $type . ":" . $log);
        } else {
            fwrite($file, "\r###" . $type . ":" . $log);
        }
        fclose($file);
    }

    private function curlPostRequest($url,$headers,$postdata)
    {
        try{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));
            $result = curl_exec($ch);



            if (!curl_errno($ch)) {
                $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            } else {
                $http_code = 0;
            }
            curl_close($ch);
            return ['http_code' => intval($http_code), 'data' => $result];

        }catch (\Exception $e){

            echo $e->getMessage();
        }
    }

    public function getCurlGetResponse($url,$headers)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 150);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    private function getRascToken(){

        date_default_timezone_set("Asia/Dhaka");
        $token = RjscToken::where('status',1)->first();
        if($token != null){
            $stored_token = $token->token;
            $exp_time = strtotime($token->exp_time);
            $current_time = strtotime("+10 minutes", time());

            if($exp_time > $current_time){
                return $stored_token;
            }
        }


        $url = config('constant.rjsc_api_base_url') . 'token/key';
        $headers = array(
            'client_id: '.config('constant.rjsc_client_id'),
            'Content-Type: application/json'
        );
        $postdata = array(
            'api_client_id' => config('constant.rjsc_client_id'),
            'secret_code' => config('constant.rjsc_secret_code'),
        );



        $response = $this->curlPostRequest($url,$headers,$postdata);



        if($response['http_code'] == 200){
            $response = json_decode($response['data']);
            $access_token = $response->accessToken;
            $expires = date('Y-m-d H:i:s',strtotime($response->expires));


            $RjscTokenObj = RjscToken::firstOrNew(['status' => 1]);
            $RjscTokenObj->token = $access_token;
            $RjscTokenObj->exp_time = $expires;
            $RjscTokenObj->created_at = date('Y-m-d H:i:s',time());
            $RjscTokenObj->save();

            return $access_token;
        }

        return '';
    }

    private function divisionList($requestData)
    {
        $inpData = $requestData['requestData'];
        $access_token = $this->getRascToken();


        if($access_token){

            $url = config('constant.rjsc_api_base_url') . 'api/info/divisions';
            $headers = array(
                'token_key: '.$access_token,
                'client_id: '.config('constant.rjsc_client_id')
            );

            $response = $this->getCurlGetResponse($url,$headers);
            $response = json_decode($response,true);
            $response = $response['_embedded']['divisionDTOList'];

            $data['rjsc']['responseType'] = 'DIVISION_RESPONSE';
            $data['rjsc']['responseStatus']['responseCode'] = 1;
            $data['rjsc']['responseStatus']['responseData'] = $response;
            $data['rjsc']['responseStatus']['message'] = '';
        }else{
            $data['rjsc']['responseType'] = 'DIVISION_RESPONSE';
            $data['rjsc']['responseStatus']['responseCode'] = 0;
            $data['rjsc']['responseStatus']['responseData'] = array();
            $data['rjsc']['responseStatus']['message'] = 'Invalid request';
        }
        header('Content-type: application/json');
        echo json_encode($data);
        exit();
    }

    private function rjscOfficeList($requestData)
    {
        $inpData = $requestData['requestData'];
        $access_token = $this->getRascToken();

        if($access_token){

            $url = config('constant.rjsc_api_base_url') . 'api/info/offices';
            $headers = array(
                'token_key: '.$access_token,
                'client_id: '.config('constant.rjsc_client_id')
            );

            $response = $this->getCurlGetResponse($url,$headers);
            $response = json_decode($response,true);

            $response = $response['_embedded']['officeInfoDTOList'];

            $data['rjsc']['responseType'] = 'RJSC_OFFICE_LIST_RESPONSE';
            $data['rjsc']['responseStatus']['responseCode'] = 1;
            $data['rjsc']['responseStatus']['responseData'] = $response;
            $data['rjsc']['responseStatus']['message'] = '';
        }else{
            $data['rjsc']['responseType'] = 'RJSC_OFFICE_LIST_RESPONSE';
            $data['rjsc']['responseStatus']['responseCode'] = 0;
            $data['rjsc']['responseStatus']['responseData'] = array();
            $data['rjsc']['responseStatus']['message'] = 'Invalid request';
        }
        header('Content-type: application/json');
        echo json_encode($data);
        exit();
    }

    private function districtList($requestData)
    {
        $inpData = $requestData['requestData'];
        $access_token = $this->getRascToken();

        if($access_token){

            $url = config('constant.rjsc_api_base_url') . 'api/info/districts';
            $headers = array(
                'token_key: '.$access_token,
                'client_id: '.config('constant.rjsc_client_id')
            );

            $response = $this->getCurlGetResponse($url,$headers);
            $response = json_decode($response,true);
            $response = $response['_embedded']['districtDTOList'];

            $data['rjsc']['responseType'] = 'DISTRICT_RESPONSE';
            $data['rjsc']['responseStatus']['responseCode'] = 1;
            $data['rjsc']['responseStatus']['responseData'] = $response;
            $data['rjsc']['responseStatus']['message'] = '';
        }else{
            $data['rjsc']['responseType'] = 'DISTRICT_RESPONSE';
            $data['rjsc']['responseStatus']['responseCode'] = 0;
            $data['rjsc']['responseStatus']['responseData'] = array();
            $data['rjsc']['responseStatus']['message'] = 'Invalid request';
        }
        header('Content-type: application/json');
        echo json_encode($data);
        exit();
    }

    private function companyTypeList($requestData)
    {
        $inpData = $requestData['requestData'];
        $access_token = $this->getRascToken();

        if($access_token){

            $url = config('constant.rjsc_api_base_url') . 'api/info/entity_types';
            $headers = array(
                'token_key: '.$access_token,
                'client_id: '.config('constant.rjsc_client_id')
            );

            $response = $this->getCurlGetResponse($url,$headers);
            $response = json_decode($response,true);


            $response = $response['_embedded']['entityTypeDTOList'];

            $data['rjsc']['responseType'] = 'COMPANY_TYPE_RESPONSE';
            $data['rjsc']['responseStatus']['responseCode'] = 1;
            $data['rjsc']['responseStatus']['responseData'] = $response;
            $data['rjsc']['responseStatus']['message'] = '';
        }else{
            $data['rjsc']['responseType'] = 'COMPANY_TYPE_RESPONSE';
            $data['rjsc']['responseStatus']['responseCode'] = 0;
            $data['rjsc']['responseStatus']['responseData'] = array();
            $data['rjsc']['responseStatus']['message'] = 'Invalid request';
        }
        header('Content-type: application/json');
        echo json_encode($data);
        exit();
    }

    private function positionList($requestData)
    {
        $inpData = $requestData['requestData'];
        $ref_id = intval($inpData['ref_id']); // Company Type ID 1/2

        $access_token = $this->getRascToken();

        if($access_token){

            $url = config('constant.rjsc_api_base_url') . 'api/info/position/'.$ref_id;
            $headers = array(
                'token_key: '.$access_token,
                'client_id: '.config('constant.rjsc_client_id')
            );

            $response = $this->getCurlGetResponse($url,$headers);
            $response = json_decode($response,true);

            $response = $response['_embedded']['positionDTOList'];

            $data['rjsc']['responseType'] = 'POSITION_BY_COM_TYPE_RESPONSE';
            $data['rjsc']['responseStatus']['responseCode'] = 1;
            $data['rjsc']['responseStatus']['responseData'] = $response;
            $data['rjsc']['responseStatus']['message'] = '';
        }else{
            $data['rjsc']['responseType'] = 'POSITION_BY_COM_TYPE_RESPONSE';
            $data['rjsc']['responseStatus']['responseCode'] = 0;
            $data['rjsc']['responseStatus']['responseData'] = array();
            $data['rjsc']['responseStatus']['message'] = 'Invalid request';
        }
        header('Content-type: application/json');
        echo json_encode($data);
        exit();
    }

    public function a(){
        echo 'A ';
        return $this;
    }
    public function b(){
        echo 'B ';
        return $this;
    }
    public function c(){
        echo 'C ';
        return $this;
    }


    public function apiJson()
    {
            $ipn = '{"UserId":"1001","Password":"10012002","TransId":"190318010374","PayMode":"A02","TransTime":"2019-03-17 18:00:00","RefTranNo":"468739002","RefTransTime":"2019-03-17 18:00:00","TransStatus":"PAID","TransAmount":"250","PayAmount":"290","DestAccount":[{"AccountNo":222333444,"Amount":100},{"AccountNo":222333445,"Amount":100},{"AccountNo":222333446,"Amount":50}]}';








//        $obj = new RjscController();
//        $obj->a()->b()->c();
//
//        exit;









        $string = '{"nc_cert_no":"2019253741","nc_submission_no":"2019140753","oss_app_id":"NC-04Apr2019-00002","req_json_data":{"registrationData":{"generalInfo":{"entityName":"Sinan Consulting Services Limited","entityType":"1","liabilityType":1,"entityAddress":"N.H.B 5/2, FLAT-B/3 (Opposite to Police Staff College), Mirpur-14, Kafrul, Dhaka","entityEmail":"siraj@batworld.com","entityDistrict":1,"mainBusinessObjective":"Consultancy services to all kind of services.","businessSector":1,"businessSubSector":8,"authorizedCapital":"10000000","numberOfShare":1000000,"valueOfEachShare":10,"minimumNumberOfDirectors":3,"maximumNumberOfDirectors":5,"quorumOfAgm":3,"quorumOfAgmInWord":"Three","quorumOfBoardOfDirectorsMeeting":3,"quorumOfBoardOfDirectorsMeetingInWord":"Three","durationChairmanship":3,"durationMd":3},"qualificationShareEachDirector":{"numberOfQualificationShare":"100","valueOfEachShare":"10","nameOfWitness":"Foyezul Islam","witnessAddress":"NHB 5/2, B-3, Mirpur 14, Kafrul","witnessDistrict":1},"listOfDirectors":[{"name":"Mohammad Sirajul Islam","formarName":"","fathersName":"Mohammad Shaidul Alam","mothersName":"Jahanara Begum","residentialAddress":"House No. NHB 5/2, Flat No. B/3, Section Mirpur 14 (Opposite of Police Staff College). \r\nKafrul.","residentialDistrict":1,"permanentAddress":"Vill: Noakhala (Dorzi Bari), Post: Yeachin Hazir Bazar, P.S.:Chatkhil,","permanentDistrict":27,"mobile":"01712411749","email":"sirajulislamit@gmail.com","nationality":13,"otherNationality":0,"dateOfBirth":"01/03/1989","tin":2147483647,"position":"2","signingAgreementOfShare":"0","nominatingEntity":null,"dateOfAppoinment":null,"otherBusinessOccupation":"","directorshipInOtherCompany":"","numberOfSubscribedShares":"10000","nationalId":"7511047745023","representedSerialNo":""},{"name":"Israt Jahan","formarName":"","fathersName":"Rofiqul Islam","mothersName":"Rokhsana Pervin","residentialAddress":"House No. NHB 5/2, Flat No. B/3 Section Mirpur 14 (Opposite of Police Staff College). \r\nKafrul","residentialDistrict":1,"permanentAddress":"Vill: Noakhala (Dorzi Bari), Post: Yeachin Hazir Bazar, P.S.:Chatkhil, ","permanentDistrict":27,"mobile":"01739576694","email":"isratjahanrakhe@gmail.com","nationality":13,"otherNationality":0,"dateOfBirth":"09/12/1987","tin":0,"position":"2","signingAgreementOfShare":"0","nominatingEntity":0,"dateOfAppoinment":null,"otherBusinessOccupation":"","directorshipInOtherCompany":"","numberOfSubscribedShares":"10000","nationalId":"1232323232","representedSerialNo":""},{"name":"Foyezul Islam","formarName":"","fathersName":"Mohammad Shaidul Alam","mothersName":"Jahanara Begum","residentialAddress":"House No. NHB 5/2, Flat No. B/3 Section Mirpur 14 (Opposite of Police Staff College). \r\nKafrul\r\n","residentialDistrict":1,"permanentAddress":"Vill: Noakhala (Dorzi Bari), Post: Yeachin Hazir Bazar, P.S.:Chatkhil","permanentDistrict":27,"mobile":"01712110884","email":"foyezulislam48@gmail.com","nationality":13,"otherNationality":0,"dateOfBirth":"10/04/1990","tin":0,"position":"2","signingAgreementOfShare":"0","nominatingEntity":0,"dateOfAppoinment":null,"otherBusinessOccupation":"","directorshipInOtherCompany":"","numberOfSubscribedShares":"10000","nationalId":"1212121212","representedSerialNo":""}],"particularsOfCorporateSubscriberList":[],"witnessList":[{"name":"Foyezul Islam","address":"House No. NHB 5/2, Flat No. B/3 \r\nSection Mirpur 14 (Opposite of Police Staff College). \r\nKafrul, Dhaka-1216","phone":"01712110884","nationalId":"9898989898"},{"name":"Faridul Islam","address":"House No. NHB 5/2, Flat No. B/3 \r\nSection Mirpur 14 (Opposite of Police Staff College). \r\nKafrul, Dhaka-1216","phone":"01710953337","nationalId":"8989898989"}],"filingBy":{"name":"Mohammad Sirajul Islam","position":2,"address":"House No. NHB 5/2, Flat No. B/3 \r\nSection Mirpur 14 (Opposite of Police Staff College). \r\nKafrul","fillingDistrict":1},"registrationSignedBy":{"name":"Mohammad Sirajul Islam","position":"2","organization":"Sinan Consulting Services Limited","address":"House No. NHB 5/2, Flat No. B/3 \r\nSection Mirpur 14 (Opposite of Police Staff College). \r\nKafrul","signedByDistrict":"1"},"aoaClauseList":[{"clauseTitle":3,"clauseDetails":"The Company is Private Limited Company within the meaning of section 2 (1) Clause\r\n(Q) of the Companies Act, 1994 and accordingly:\r\n\r\ni) No invitation shall be issued to the public to subscribe for any shares, debentures\r\nor debenture stock of the Company;\r\n\r\nii) The number of members of the Company (exclusive of the persons in the\r\nemployment of the Company) shall be limited to 50, provided that for the purposes\r\nof these provisions when two or more persons jointly hold one or more shares in the\r\nCompany, they shall be treated as a single member, and the right to transfer shares\r\nin the Company is restricted in the manner and to the extent hereinafter appearing.","serialId":1},{"clauseTitle":35,"clauseDetails":"The business of the Company shall be commenced from the date of incorporation of\r\nthe company.","serialId":2},{"clauseTitle":5,"clauseDetails":"each with the\r\nrights, privileges and conditions attaching thereto as are provided by the regulations\r\nof the Company for the time being, with power to increase and reduce the capital of\r\nthe Company and to divide this shares in the capital for the time being into several\r\nclasses and to attach thereto respectively such preferential, deferred, qualify or\r\nspecial rights, privileges or conditions as may be determined by or in accordance\r\nwith the regulations of the Company and to vary, modify or abrogate any such right,\r\nprivileges or conditions in such manner as may for the time being be provided by the\r\nregulations of the Company. Subject to the provisions of the Articles, the share\r\nshall be under the control of the directors who may allot or otherwise dispose of the\r\nsame.","serialId":3},{"clauseTitle":6,"clauseDetails":"The certificate of title to shares and debentures shall be issued under the common\r\nseal of the Company and signed by the Managing Director. \r\n\r\nEvery member shall be entitled, free of charge, to one certificate for all the shares\r\nand debentures registered in his/her name. Every certificate shall specify the\r\nnumber and denoting numbers of the shares or debentures in respect of which it\r\nwas issued and the amount paid-up thereon.\r\n\r\nIf any share certificate shall be defaced, worn out, destroyed or lost it any be\r\nreissued on such evidence being produced and on such indemnity (if any) being\r\ngiven as the Directors require and (in case of defacement or wearing out) on\r\ndelivering of the old certificate and on payment of such sum not exceeding Tk.\r\n15.00 (Taka Fifteen) as the Directors may from time to time require.","serialId":4},{"clauseTitle":36,"clauseDetails":"The company shall have a lien on every share (not being a fully paid share) for all\r\nmoneys whether presently payable or not, called or payable at a fixed time in\r\nrespect of those shares. The company shall also have lien on all shares (other than\r\nfully paid shares) standing registered in the name of a single person for all moneys\r\npayable by him or his estate to the company, but the directors may at any time\r\ndeclare any share to be wholly or partly exempt from the provisions of this clause.\r\nThe company¿s lien shall also extend to the dividend payable thereon. The\r\npurchase¿s name shall be registered as the holder or the share and he shall not be\r\nbound to see the application of the purchase money, nor shall his title to the share\r\nbe affected by reason of any irregularity or invalidity in the proceedings with\r\nreference to the sale.","serialId":5},{"clauseTitle":36,"clauseDetails":"The Company may sell or otherwise dispose of in such manner as the Directors may\r\nthink fit any share on which the company has a lien, but no sale shall be made\r\nunless the sum in respect of which the lien exists, in presently payable nor until the\r\nexpiration of fourteen days after a notice in writing stating and demanding the\r\npayment of the amount in respect of which the lien exist as is presently payable,\r\nhas been given to the registered holder for the time being of the share, or the\r\nperson entitled to the share by reason of death or insolvency of the holder of the\r\nshare. The proceeds of the sale shall be applied in payment of the amount in\r\nrespect of which the lien exists, as is presently payable, and the residue shall\r\n(subject to like lien for sum not presently payable as existing upon the share prior to\r\nthe sale) be paid to the person entitled to the share at the date of sale.","serialId":6},{"clauseTitle":7,"clauseDetails":"With the approval of the Board of directors any share may be transferred at anytime\r\nby a member to his or her son, daughter, brother-in-law, father, brother, mother or\r\nto his wife or her husband.\r\n\r\nNo transfer of any share shall be made or registered without the previous sanction\r\nof the directors who may without assigning any reason decline to give any such\r\nsanction, and shall decline any transfer involving contravention of clause 3(b) of\r\nthese articles. No share shall be transferred to any outsiders as long as any member\r\nis willing to purchase the same at a fair value to be determined by the Directors in a\r\nBoard meeting. The Instrument of Transfer of shares shall be accompanied by the\r\ncertificate of shares for transfer of shares previously approved by the Board of\r\nDirectors. \r\n","serialId":7},{"clauseTitle":71,"clauseDetails":"The Company may from time to time subject to these presents and the relevant\r\nprovisions of law, in the General Meeting increase its share capital by creation of\r\nnew shares of such amount as it thinks expedient upon such terms and conditions\r\nand with such rights and privileges as may be determined by the Board of Directors.","serialId":8},{"clauseTitle":71,"clauseDetails":"Subject to any direction to the contrary that may be given by the resolution\r\nsanctioning the increasing of share capital and any directive by the Government\r\nand/or Bangladesh Bank, all new shares shall before issue be offered to members in\r\nproportion, as nearly as circumstances admit to the amount of the existing shares\r\nthen held by them. The offer shall be made by notice specifying the number of shares offered and limiting a time within which the offer if not accepted will be\r\ndeemed to be declined and after the expiration of that time, or on the receipt of an\r\nintimation from the person to whom the offer is made that he declines to accept the\r\nshares offered, the Directors may dispose of the same in such manner as they think\r\nmost beneficial to the Company.","serialId":9},{"clauseTitle":9,"clauseDetails":"The Directors may at their discretion borrow any sum(s) of money for the purpose of\r\nthe Company subject to the provisions of these presents.\r\n\r\nThe Directors may raise and secure the payments of such sum(s) in such manner\r\nand upon such terms and conditions in all respects as they think fit and in particular\r\nby the issue of bonds (perpetual or redeemable) or any mortgage or charge or other\r\nsecurity on the undertaking of the whole or any part of the property of the\r\nCompany and the Directors may on behalf of the Company guarantee the whole or\r\nany part of any loans or debts incurred by the Company.","serialId":10},{"clauseTitle":10,"clauseDetails":"The Company shall within a period of not less than one month nor more than six\r\nmonths from the date at which the Company shall be entitled to commence business\r\nhold a General Meeting of the Company, which shall be called the Statutory Meeting\r\nand in connection therewith the Directors shall comply with the provisions of\r\nSection 83 of the Act. The General Meeting of the Company may take place outside\r\nBangladesh.","serialId":11},{"clauseTitle":10,"clauseDetails":"A General Meeting of the Company shall be held within eighteen (18) months from\r\nthe date of incorporation of the company thereafter once at least in every calendar\r\nyear at such time and place as may be determined by the Directors provided that no\r\ninterval longer than fifteen (15) months shall be allowed to elapse between each\r\nordinary general meetings. Such General Meetings shall be called ordinary general\r\nmeetings. All other meetings of the Company other than the Statutory Meeting shall\r\nbe called Extra-ordinary General Meetings.","serialId":12},{"clauseTitle":11,"clauseDetails":"Two (2) members entitled to vote and present in person shall be a quorum for a\r\nGeneral Meeting and no business shall be transacted at any General Meeting unless\r\nthe quorum requisite be present at the commencement of the business.\r\n\r\nThe Board of Directors shall elect one of them to act as the Chairman of the\r\nmeeting and in default of their doing so the members present shall choose one of\r\nthe Directors to take the chair and if no Director is present or those present are not\r\nwilling to take the chair the members shall choose one of the members to be the\r\nChairman of the meeting. \r\n","serialId":13},{"clauseTitle":12,"clauseDetails":"2 (Two) Members present in person and qualified to vote shall form a Quorum in any\r\ngeneral meeting. Whenever the Board of Directors think it necessary, it may call a\r\nGeneral Meeting, whether ordinary or extra-ordinary at such time (subject to the\r\nprovisions of Section 84 of the Act) and place as the Board thinks fit. The Chairman\r\nof the company shall preside at every General Meeting. In absence of Chairman the\r\nBoard of Directors shall choose another to be the Chairman of that meeting. In any general meeting a resolution put to vote of the members shall be decided by a show\r\nof hands unless a poll is demanded in accordance with the provisions of clause (c)\r\nof sub-section (I) of section 85 of the Companies Act, 1994.","serialId":14},{"clauseTitle":14,"clauseDetails":"On a show of hands every member present in person shall have one vote, on a poll\r\nvotes may be given either personally or by proxy or by constituted attorney, and\r\nevery member shall have one vote proxy as a resolution of its directors in\r\naccordance with the provisions of section 85 of the Companies Act is in force. In\r\ncase of any equalisation of votes the Chairman of the meeting each will, have a\r\ncasting vote in addition tot he vote or votes to which they entitled to vote as a\r\nmember. \r\n\r\nNo member shall be entitled to vote unless all instalments or calls or other sum or\r\nsums presently payable by him, in respect of his holding shares in the company has\r\nbeen paid.","serialId":15},{"clauseTitle":15,"clauseDetails":"Unless Otherwise determined by the company in general meeting the number of\r\ndirectors shall not be less than 2( Two ) and not more than 10( Ten ).The following\r\npersons shall be the first directors of the company unless anyone of them voluntarily\r\nresigns the said office or otherwise removed therefrom under the provisions of\r\nsection 108(1) of the companies Act, 1994.","serialId":16},{"clauseTitle":16,"clauseDetails":"The qualification of a Director shall be holding of share of total nominal value of Tk.\r\n1,000 (One Thousand) in the Capital of the company in his/her own name alone and\r\nnot jointly with any other/others.\r\n\r\nThe remuneration of a director shall not exceed Tk. 500/- (Taka five hundred) each\r\nfor meeting of the directors attended by him together with such travelling and other\r\nexpenses as may be incurred for attending the meeting. \r\n","serialId":17},{"clauseTitle":13,"clauseDetails":"2 (Two) Members present in person and qualified to vote shall form a Quorum in any\r\ngeneral meeting. Whenever the Board of Directors think it necessary, it may call a\r\nGeneral Meeting, whether ordinary or extra-ordinary at such time (subject to the\r\nprovisions of Section 84 of the Act) and place as the Board thinks fit. The Chairman\r\nof the company shall preside at every General Meeting. In absence of Chairman the\r\nBoard of Directors shall choose another to be the Chairman of that meeting.","serialId":18},{"clauseTitle":17,"clauseDetails":"The business of the Company shall be managed by the Directors, who may pay all\r\nexpenses incurred in setting up and registering the Company, and may exercise all\r\nsuch powers of the Company as are not, by the Act or any statutory modification\r\nthereof for the time being in force or by these Articles, required to be exercised by\r\nthe Company in General Meeting, subject nevertheless to any regulation of these\r\nArticles, to the provisions of the Act and to such Article being consistent with the\r\naforesaid Articles, as may be prescribed by the Company in its General Meeting. No\r\nregulation made by the Company in General Meeting shall invalidate any prior act of\r\nthe Directors which would have been valid if that Article had not been made.","serialId":19},{"clauseTitle":18,"clauseDetails":"The office of Director shall be vacated if a Director:\r\n\r\na) is found to be of unsound mind by a Court of competent jurisdiction; or\r\n\r\nb) is adjudged insolvent; or\r\n\r\nc) fails to pay calls made on him in respect of shares held by him within the period\r\nas mentioned under Article 13 of these Articles; or\r\n\r\nd) absents himself from three consecutive meetings of the Directors, or for a\r\ncontinuous period of three months, whichever is longer, without leave of absence\r\nfrom the Board of Directors and without appointing an Alternate Director; or\r\n\r\ne) by notice in writing to the Company resigns his office at the Annual General\r\nMeeting of the Shareholders","serialId":20},{"clauseTitle":17,"clauseDetails":"The Directors may meet together for transaction of any business, adjourn and/or\r\notherwise regulate their meetings and proceedings as they may think fit. All\r\nmeetings of Directors shall be presided over by chairman. \r\n\r\nThe Chairman or the Managing Director or in their absence, in case of any\r\nemergency, any Director may convene a meeting of Directors. The Board of\r\nDirectors shall meet at least once every two (2) months. \r\n\r\nNotice of the meeting given to a Director at the address registered with the\r\nCompany shall be deemed to be valid notice. \r\n\r\ni) Generally Seven (7) days notice shall be given for meetings of the Directors. In\r\ncase of emergency the Chairman or Managing Director may hold a meeting of the\r\nDirectors at a shorter notice; provided consent is given by all the Directors to such\r\nshorter notice at the meeting held pursuant thereto.","serialId":21},{"clauseTitle":47,"clauseDetails":"ii) Accidental omission to give notice of any such meeting of the Directors to a\r\nDirector shall not invalidate any resolution passed at any such meeting. \r\n\r\nA director may, and Secretary on the requisition of a director shall at anytime,\r\nsummon a meeting of directors. \r\n\r\nThe quorum necessary for the transaction of the business of the directors shall be\r\n2(two). If no quorum is present within forty five (45) minutes of the time appointed\r\nfor a meeting such meeting will stand automatically adjourned to another day to be\r\ndetermined by the Board. \r\n\r\nResolutions of the Board shall be valid if passed at a meeting at which a quorum of\r\nsimple majority of the members of the Board is present.","serialId":22},{"clauseTitle":19,"clauseDetails":"Mrs. Wu Mei shall be the first Chairman of the Company for a period of 05 (Five)\r\nyears from the date of incorporation. He will preside all over the meeting and\r\nsupervision all the activities of the company. He will preside all over the meeting and\r\nsupervision all the activities of the company. The Chairman may also make such\r\nother appropriate arrangement in this regards as may be deemed required from time\r\nto time.","serialId":23},{"clauseTitle":20,"clauseDetails":"shall be the Managing Director of the Company for a period as may\r\nbe decided by the Board of Directors of the Company. The Managing Director shall\r\nbe entrusted with the responsibility for the day-to-day running and business of the\r\nCompany and the supervision and observance of the general guidelines provided by\r\nthe Board of Directors.","serialId":24},{"clauseTitle":26,"clauseDetails":"The General Meeting shall annually determine the amount of profit out of the\r\ndistributable reserves available for actual distribution (dividend) to the Shareholders.\r\nNo dividend will be paid out to the Shareholders until there has been two (2) years\r\nof positive accumulated cash flow, net of debt services.\r\n\r\nThe Company in General Meeting may declare dividends, but no dividends shall\r\nexceed the amount recommended by the Directors.\r\n\r\nThe Directors may from time to time pay to the Shareholders such interim dividends\r\nas appears to the Directors to be justified by the profits of the Company.","serialId":25},{"clauseTitle":21,"clauseDetails":"The Company shall open Bank Account with any commercial Bank/Banks and shall\r\ndraw money by cheques from the said Accounts as per the decision of Managing\r\nDirector only or by the Board of Directors of the Company from time to time.","serialId":26},{"clauseTitle":24,"clauseDetails":"The auditors of the Company shall be appointed at the General Meeting of\r\nShareholders for a period one year at a time.\r\n\r\nThe Management Team shall, under the supervision of the Board of Directors, keep\r\nand maintain complete and accurate books of accounts in which each and every\r\ntransaction of the Company shall be entered fully and accurately. Such books shall\r\nbe maintained in accordance with International Accounting Standards (IAS), and\r\nshall at all times be maintained at the registered office of the Company. The\r\naudited annual accounts shall be prepared in English.\r\n\r\nThe Company shall provide such financial information to the Shareholders as is\r\nnecessary for the Shareholders to comply with their local accounting requirements.\r\nA Shareholder may obtain access to the accounts, books and records of the\r\nCompany for the purpose of inspection.","serialId":27},{"clauseTitle":23,"clauseDetails":"(a) A notice may be given by the Company to the Shareholders or the Directors\r\neither personally or through registered post to their registered addresses. The\r\nnotice may be served by confirmed telex or facsimile to numbers provided by the\r\nDirectors and the Shareholders to the Company for the giving of notices to them.\r\n\r\n(b)Where a notice is sent by registered post, service of the notice shall be deemed\r\nto be effected by properly addressing, prepaying, registering and posting a\r\nregistered letter containing the notice and, unless the contrary is proved, to have\r\nbeen effected at the time at which the letter would be delivered in the ordinary\r\ncourse of post.","serialId":28},{"clauseTitle":68,"clauseDetails":"Each Shareholder shall, and shall cause its Affiliates and any employee, officer,\r\ndirector, adviser or agent of any of the foregoing to maintain confidentiality and not\r\ndisclose all or any part of any information, reports, documents, specifications,\r\nblueprints, analyses, computer programs or files and/or photographs or drawings or\r\nother materials provided by any other Shareholder (a \"Disclosing Shareholder\"),\r\ndeveloped by the Company (collectively referred to as the \"Information\").","serialId":29},{"clauseTitle":27,"clauseDetails":"The Seal of the Company shall not be affixed to any instrument except with the\r\nauthority of a resolution of the Board of Directors, and in the presence of at least\r\none Director and the Secretary or such other person as the Directors may appoint\r\nfor such purpose. Such Director and Secretary or other person as aforesaid shall\r\nsign every instrument to which the seal of the Company is so affixed in their\r\npresence.","serialId":30},{"clauseTitle":22,"clauseDetails":"The Board of Directors may from time to time appoint any suitable and acceptable","serialId":31},{"clauseTitle":25,"clauseDetails":"The company shall comply with the provision of section 36 of the Companies Act,\r\n1994 as making of annual returns.","serialId":32},{"clauseTitle":28,"clauseDetails":"Every Director, member of the Management Team, agent, auditor, Secretary and\r\nother officer for the time being of the Company shall be indemnified out of the\r\nassets of the Company against any liability incurred by him in defending any\r\nproceedings, whether civil or criminal, in which judgement is given in his favour, or in\r\nwhich he is acquitted, or in connection with any application under Section 396 of\r\nthe Act in which relief is granted to him by the Court.","serialId":33},{"clauseTitle":29,"clauseDetails":"The Chairman, Managing Director, Director, Advisor, Manager, Secretary, Auditors,\r\nAccountants, Officers, Agents or other person employed in the business of the shall\r\nhave to observe strict secrecy respecting all matters which may come to his\r\nknowledge in the discharge of the duties when required to do so by resolution of the\r\ncompany or by court of law or where the person connected is required to do so in\r\norder to comply with any provision of the law or in these presents.","serialId":34}],"moaObjectiveList":[{"serialNumber":1,"objectiveDetail":"To carry on the business of manufacturers, trading, buy, sell, exporters, importers, Marketing, indenters, commission agents, selling agents, distributors, broker, stockiest, merchants, wholesalers, retailers, buyers, general order suppliers and dealers in general merchandise, commodities, articles and goods all description whatsoever.","dmId":4},{"serialNumber":2,"objectiveDetail":"To carry in Bangladesh or elsewhere trade, business manufacture, and/or to act, as business agent, managing agents, selling agents, local agents, sub-agents, sole agents, handling agents, commission agents, indenting agents, indenters, stevedores, brokers for the benefit of the company.","dmId":4},{"serialNumber":3,"objectiveDetail":"To manufacture, repack, import, export, stock, supply, and distribute pharmaceuticals, nutritional, herbals and other medicinal and health care products.","dmId":4},{"serialNumber":4,"objectiveDetail":"To carry on the business of general trading, export and import of all commercial and industrial permissible items, dealers, indenter, manufacturer, general supplier, buying and selling agents, wholesale and distributors, dealers, general merchandiser, carrying contractor of all and every kind of general and special products, goods substance and materials.","dmId":4},{"serialNumber":5,"objectiveDetail":"To attain the business objectives company may enter into Partnership, Jointventure, take over or Amalgamate with any other company and also to take Loans from Bank/other Financial Institutions in such a manner as may company thinks fit.","dmId":4},{"serialNumber":6,"objectiveDetail":"To borrow or raise money or secure the payment of money on such term as the company may consider expedient, including by issue or sale of shares, stock, bonds, debentures, other securities and obligations, perpetual or terminable and or redeemable or otherwise and to secure the same by mortgage, charge or lien on the undertaking and all or any of the real and personal property and assets, present or future and all or any of the uncalled capital for the time being of the company, any to issue and create at par or at a premium per discount, and for such consideration and win and subject to such rights, power, privileges and conditions as may be thought fit, mortgage, charges, memoranda or deposit, debentures or debenture stock, either permanent or redeemable or repayable, and collaterally or future to secure any securities of the company by a trust deed or other assurance.","dmId":4},{"serialNumber":7,"objectiveDetail":"To mortgage the property and assets of the company as securities for loans and/or any credit facilities to be given to any associate company or companies or third party and also to give guarantee securing liabilities of such associate company or companies and/or third party. To attain the business objectives company as subsidiary of Aviation Support Limited enter into Contract, Partnership, Joint venture, take over or Amalgamate with any other company and also to take Loans from Bank/other Financial Institutions in such a manner as company may think fit.","dmId":4}],"archiveDocInfo":{"documentId":"49","moaPages":"15","aoaPages":"15"},"requestInfo":{"entryBy":"","remoteAddress":"","ossApplicationId":"NC-04Apr2019-00002"}}}}';
        $string = json_decode($string,true);


//        echo '<pre>';
//        #print_r($string['req_json_data']['registrationData']['qualificationShareEachDirector']);
//        print_r($string['req_json_data']['registrationData']['qualificationShareEachDirector']);
//        echo '</pre>';
//        exit;


        echo 'qualificationShareEachDirector <br/><br/>';
        foreach($string['req_json_data']['registrationData']['qualificationShareEachDirector'] as $key => $value){
            echo $key.' :'.$value;

            if(mb_detect_encoding($value, 'ASCII', true)){
               echo '    : ASCI VERIFIED';
            }

            echo '<br/>';
            echo '<br/>';
        }

        exit;


        if(mb_detect_encoding($string, 'ASCII', true)){
            echo $string;exit;
        }
        exit;

        $json['nc_save_id'] = "201902362";
        $json['nc_request_by'] = "Arifur  Rahman";
        $json['remarks'] = "";
        $json['branch_code'] = "44057";
        $json['account_info'] = array(
            array(
                'account_no' => '0002634313655',
                'balance' => 'XXXXXXXX',
                'deposit' => '200',
                'tran_date' => '2019-03-05 14:17:27',
                'tran_id' => '19030501098792011'),
            array(
                'account_no' => '0002634313655',
                'balance' => 'XXXXXXXX',
                'deposit' => '10',
                'tran_date' => '2019-03-05 14:17:27',
                'tran_id' => '19030501098792012')
        );

        dd(json_encode($json));












        $json = '{
	"nc_cert_no": "8452554545",
	"nc_submission_no": 20190133,
	"oss_app_id": "NC-231220180020",
	"req_json_data": {
		"registrationData": {
			"generalInfo": {
				"entityName": "Bull Fight Ltd.",
				"entityType": 1,
				"liabilityType": 1,
				"entityAddress": "682/5, East Monipur",
				"entityEmail": "moudud@gmail.com",
				"entityDistrict": 1,
				"mainBusinessObjective": "One stop service",
				"businessSector": 1,
				"businessSubSector": 1,
				"authorizedCapital": 5000000,
				"numberOfShare": 20000,
				"valueOfEachShare": 100,
				"minimumNumberOfDirectors": 2,
				"maximumNumberOfDirectors": 10,
				"quorumOfAgm": 2,
				"quorumOfAgmInWord": "two",
				"quorumOfBoardOfDirectorsMeeting": 5,
				"quorumOfBoardOfDirectorsMeetingInWord": "five",
				"durationChairmanship": 3,
				"durationMd": 5
			},
			"qualificationShareEachDirector": {
				"numberOfQualificationShare": 100000,
				"valueOfEachShare": 100,
				"nameOfWitness": "Mostafizur Rahman",
				"witnessAddress": "Mirpur,Dhaka",
				"witnessDistrict": 1
			},
			"listOfDirectors": [{
					"name": "Jony",
					"formarName": "Hassan",
					"fathersName": "Abdul",
					"mothersName": "Begum",
					"residentialAddress": "Mirpur,Dhaka",
					"residentialDistrict": 29,
					"permanentAddress": "Mirpur,Dhaka",
					"permanentDistrict": 12,
					"mobile": "01786273127",
					"email": "moudud@gmail.com",
					"nationality": 1,
					"otherNationality": 5,
					"dateOfBirth": "12/11/1978",
					"tin": "1234677778",
					"position": 4,
					"signingAgreementOfShare": 1,
					"nominatingEntity": 1,
					"dateOfAppoinment": "12/11/2019",
					"otherBusinessOccupation": "Job",
					"directorshipInOtherCompany": "business automation",
					"numberOfSubscribedShares": 1000000,
					"nationalId": "122345565",
					"representedSerialNo": 0
				},
				{
					"name": "Choton",
					"formarName": "Podder",
					"fathersName": "Abdul_1",
					"mothersName": "Begum_1",
					"residentialAddress": "Mirpur,Dhaka",
					"residentialDistrict": 29,
					"permanentAddress": "Mirpur,Dhaka",
					"permanentDistrict": 12,
					"mobile": "01786273127",
					"email": "moudud@gmail.com",
					"nationality": 1,
					"otherNationality": 5,
					"dateOfBirth": "12/11/1978",
					"tin": "1234677778",
					"position": 3,
					"signingAgreementOfShare": 1,
					"nominatingEntity": 2,
					"dateOfAppoinment": "12/11/2019",
					"otherBusinessOccupation": "Job",
					"directorshipInOtherCompany": "business automation",
					"numberOfSubscribedShares": 2000000,
					"nationalId": "122345565",
					"representedSerialNo": 1
				}
			],
			"particularsOfCorporateSubscriberList": [{
				"nameOfCorporateBody": "Nittodin",
				"representedBy": "Zarah",
				"address": "Pollobi,Dhaka",
				"numberOfSubscribedShare": 10000,
				"district": 2,
				"serialId": 1
			}],
			"witnessList": [{
					"name": "name1",
					"address": "pollobi",
					"phone": "01786273127",
					"nationalId": "12734747"
				},
				{
					"name": "name2",
					"address": "pollobi",
					"phone": "01786273127",
					"nationalId": "12734747"
				}
			],
			"filingBy": {
				"name": "Junaid",
				"position": 2,
				"address": "pollibi,dhaka",
				"fillingDistrict": 5
			},
			"registrationSignedBy": {
				"name": "Junaid",
				"position": 2,
				"organization": "BAT",
				"address": "pollibi,dhaka",
				"signedByDistrict": 5
			},

			"aoaClauseList": [{
					"clauseTitle": 2,
					"clauseDetails": "Abcd is very strong",
					"serialId": 1
				},
				{
					"clauseTitle": 3,
					"clauseDetails": "Abcd is very strong",
					"serialId": 2
				},
				{
					"clauseTitle": 4,
					"clauseDetails": "Abcd is very strong",
					"serialId": 3
				},
				{
					"clauseTitle": 5,
					"clauseDetails": "Abcd is very strong",
					"serialId": 4
				}
			],

			"moaObjectiveList": [{
					"serialNumber": 1,
					"objectiveDetail": "I am good guy"
				},
				{
					"serialNumber": 2,
					"objectiveDetail": "I am good guy"
				},
				{
					"serialNumber": 3,
					"objectiveDetail": "I am good guy"
				}
			],
			"archiveDocInfo": {
				"documentId": 49,
				"moaPages": 2,
				"aoaPages": 3
			},
			"requestInfo": {
				"entryBy": "OSS_BIDA",
				"remoteAddress": "192.168.152.163",
				"ossApplicationId": "NC-231220180020"
			}
		}
	}
}';

//        echo '<pre>';
//        print_r(json_decode($json));
//        echo '</pre>';
//        exit;



        $rData0['nc_cert_no'] = '166999';
        $rData0['nc_submission_no'] = 201902361;
        $rData0['oss_app_id'] = 'NC-23442385';


        $rData['generalInfo'] = array(
                    'entityName' => 'Bull Fight Ltd',
                    'entityType' => 1,
                    'liabilityType' => 1,
                    'entityAddress' => '682/5, East Monipur',
                    'entityEmail' => 'moudud@gmail.com',
                    'entityDistrict' => 1,
                    'mainBusinessObjective' => 'One stop service',
                    'businessSector' => 1,
                    'businessSubSector' => 1,
                    'authorizedCapital' => 5000000,
                    'numberOfShare' => 20000,
                    'valueOfEachShare' => 100,
                    'minimumNumberOfDirectors' => 2,
                    'maximumNumberOfDirectors' => 10,
                    'quorumOfAgm' => 2,
                    'quorumOfAgmInWord' => 'two',
                    'quorumOfBoardOfDirectorsMeeting' => 5,
                    'quorumOfBoardOfDirectorsMeetingInWord' => 'five',
                    'durationChairmanship' => 3,
                    'durationMd' => 5
                );
        $rData['qualificationShareEachDirector'] = array(
                            'numberOfQualificationShare' => 100000,
                            'valueOfEachShare' => 100,
                            'nameOfWitness' => 'Mostafizur Rahman',
                            'witnessAddress' => 'Mirpur,Dhaka',
                            'witnessDistrict' => 1
                        );


        $rData['listOfDirectors'] =  array(
            array(
                'name' => 'Jony',
                'formarName' => 'Hassan',
                'fathersName' => 'Abdul',
                'mothersName' => 'Begum',
                'residentialAddress' => 'Mirpur,Dhaka',
                'residentialDistrict' => 29,
                'permanentAddress' => 'Mirpur,Dhaka',
                'permanentDistrict' => 12,
                'mobile' => '01786273127',
                'email' => 'moudud@gmail.com',
                'nationality' => 1,
                'otherNationality' => 5,
                'dateOfBirth' => '12/11/1978',
                'tin' => '1234677778',
                'position' => 4,
                'signingAgreementOfShare' => 1,
                'nominatingEntity' => 1,
                'dateOfAppoinment' => '12/11/2019',
                'otherBusinessOccupation' => 'Job',
                'directorshipInOtherCompany' => 'business automation',
                'numberOfSubscribedShares' => 1000000,
                'nationalId' => '122345565',
                'representedSerialNo' => 0
            ),
            array
            (
                    'name' => 'Choton',
                    'formarName' => 'Podder',
                    'fathersName' => 'Abdul_1',
                    'mothersName' => 'Begum_1',
                    'residentialAddress' => 'Mirpur,Dhaka',
                    'residentialDistrict' => 29,
                    'permanentAddress' => 'Mirpur,Dhaka',
                    'permanentDistrict' => 12,
                    'mobile' => '01786273127',
                    'email' => 'moudud@gmail.com',
                    'nationality' => 1,
                    'otherNationality' => 5,
                    'dateOfBirth' => '12/11/1978',
                    'tin' => '1234677778',
                    'position' => 3,
                    'signingAgreementOfShare' => 1,
                    'nominatingEntity' => 2,
                    'dateOfAppoinment' => '12/11/2019',
                    'otherBusinessOccupation' => 'Job',
                    'directorshipInOtherCompany' => 'business automation',
                    'numberOfSubscribedShares' => 2000000,
                    'nationalId' => '122345565',
                    'representedSerialNo' => 1
            )
        );


        $rData['particularsOfCorporateSubscriberList'] = array(
            array(
                'nameOfCorporateBody' => 'Nittodin',
                'representedBy' => 'Zarah',
                'address' => 'Pollobi,Dhaka',
                'numberOfSubscribedShare' => 10000,
                'district' => 2,
                'serialId' => 1
            )
        );

        $rData['witnessList'] = array(
            array(
                'name' => 'name1',
                'address' => 'pollobi',
                'phone' => '01786273127',
                'nationalId' => '12734747'
            ),
            array(
                'name' => 'name2',
                'address' => 'pollobi',
                'phone' => '01786273127',
                'nationalId' => '12734747'
            )
        );

        $rData['filingBy'] = array(
            'name' => 'Junaid',
            'position' => 2,
            'address' => 'pollibi,dhaka',
            'fillingDistrict' => 5
        );

        $rData['registrationSignedBy'] = array(
            'name' => 'Junaid',
            'position' => 2,
            'organization' => 'BAT',
            'address' => 'pollibi,dhaka',
            'signedByDistrict' => 5
        );

        $rData['aoaClauseList'] = array(
            array(
                'clauseTitle' => 2,
                'clauseDetails' => 'Abcd is very strong',
                'serialId' => 1
            ),
            array(
                'clauseTitle' => 3,
                'clauseDetails' => 'Abcd is very strong',
                'serialId' => 2
            ),
            array(
                'clauseTitle' => 4,
                'clauseDetails' => 'Abcd is very strong',
                'serialId' => 3
            ),
            array(
                'clauseTitle' => 5,
                'clauseDetails' => 'Abcd is very strong',
                'serialId' => 4
            )
        );

        $rData['moaObjectiveList'] = array(
            array(
                'serialNumber' => 1,
                'objectiveDetail' => 'I am good guy',
                'dmId' => 3
            ),
            array(
                'serialNumber' => 2,
                'objectiveDetail' => 'I am good guy',
                'dmId' => 3
            ),
            array(
                'serialNumber' => 3,
                'objectiveDetail' => 'I am good guy',
                'dmId' => 3
            )
        );

        $rData['archiveDocInfo'] = array(
            'documentId' => 49,
            'moaPages' => 2,
            'aoaPages' => 3
        );

        $rData['requestInfo'] = array(
            'entryBy' => '',
            'remoteAddress' => '',
            'ossApplicationId' => 'NC-23442385'
        );
        $rReq = $rData0;
        $rReq['req_json_data']['registrationData'] = $rData;

        //        $rData['requestInfo'] = array(
//            'entryBy' => 'PHP_DEV1',
//            'remoteAddress' => '192.168.152.163',
//            'ossApplicationId' => 'NC-23442385'
//        );

        //$request = json_encode($rReq);

//        RjscRegistration::where('id', 1)
//                    ->update(
//                        [
//                            'request' => $request
//                        ]);



            echo '<pre>';
            #print_r(json_decode($str));
            print_r(json_encode($rReq));
            #print_r(json_encode($rReq));
            echo '</pre>';
            exit;

    }


}
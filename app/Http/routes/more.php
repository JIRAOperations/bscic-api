<?php

$app->get('/', 'PassportController@welcome');

$app->get('rjsc/api-request', 'RjscController@apiRequest');
$app->get('apiJson', 'RjscController@apiJson');


$app->post('update-application-status', 'BidaMohaApiController@applicationStatusUpdate');

$app->get('api/get-base64-from-url', 'FileBase64Controller@getBase64FromUrl');


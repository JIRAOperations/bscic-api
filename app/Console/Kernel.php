<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

//doe start
        \App\Console\Commands\DOE\DoeGetChallanCode::class,
        \App\Console\Commands\DOE\DoeBaseData::class,
        \App\Console\Commands\DOE\DoeBaseData2::class,
        \App\Console\Commands\DOE\DoePaymentConfirm::class,
        \App\Console\Commands\DOE\DoeDocSending::class,
        \App\Console\Commands\DOE\DoeFormOneSubmission::class,
        \App\Console\Commands\DOE\DoeFormTwoSubmission::class,
        \App\Console\Commands\DOE\DoeFormFinalSubmission::class,
        \App\Console\Commands\DOE\DoeApplicationStatusCheck::class,

        \App\Console\Commands\DOE\DoeBaseDataForShortfall::class,
        \App\Console\Commands\DOE\DoeCommentDataSending::class,
        \App\Console\Commands\DOE\DoeShortfallDocSending::class,

        //doe end
//ccie start
        \App\Console\Commands\CCIE\CcieFormSubmission::class,
        \App\Console\Commands\CCIE\CcieApplicationStatusCheck::class,
        \App\Console\Commands\CCIE\CcieGetChallanCode::class,
        \App\Console\Commands\CCIE\CciePaymentConfirm::class,
        \App\Console\Commands\CCIE\CcieShortfallSubmission::class,
        \App\Console\Commands\CCIE\CcieGetShortfallStatus::class,
        \App\Console\Commands\CCIE\CcieChallanPaymentConfirm::class,
//ccie end

        /*DCCI*/

        \App\Console\Commands\DCCI\ApplicationSubmission::class,
        \App\Console\Commands\DCCI\DCCIBankInfo::class,
        \App\Console\Commands\DCCI\DCCIPayConfirm::class,
        \App\Console\Commands\DCCI\DCCIAppplicationStatus::class,
        \App\Console\Commands\DCCI\DCCIShortfallStatus::class,
        \App\Console\Commands\DCCI\DCCICertificatePDF::class,
        \App\Console\Commands\DCCI\DCCICertificateZIP::class,







        
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

//doe start
        $schedule->command('doe:getchallancode')->cron('* * * * *');
        $schedule->command('doe:basedata')->cron('* * * * *');
        // $schedule->command('doe:basedata2')->cron('* * * * *');
        $schedule->command('doe:form1')->cron('* * * * *');
        $schedule->command('doe:form2')->cron('* * * * *');
        $schedule->command('doe:doc')->cron('* * * * *');
        $schedule->command('doe:payconfirm')->cron('* * * * *');
        $schedule->command('doe:final')->cron('* * * * *');
        $schedule->command('doe:status')->cron('* * * * *');

        $schedule->command('doe:basecomment')->cron('* * * * *');

        $schedule->command('doe:commentdatasending')->cron('* * * * *');



        $schedule->command('doe:shortfalldoc')->cron('* * * * *');
//doe end

//ccie start
        $schedule->command('ccie:formsubmission')->everyMinute()->withoutOverlapping();
        $schedule->command('ccie:status')->everyFiveMinutes()->withoutOverlapping();
        $schedule->command('ccie:getchallancode')->everyMinute()->withoutOverlapping();
        $schedule->command('ccie:payconfirm')->everyFiveMinutes()->withoutOverlapping();
        $schedule->command('ccie:challanconfirm')->everyMinute()->withoutOverlapping();
        $schedule->command('ccie:shortfallSubmission')->everyFiveMinutes()->withoutOverlapping();
        $schedule->command('ccie:getshortfallstatus')->everyFiveMinutes()->withoutOverlapping();

//ccie end

        $schedule->command('dcci:formsubmission')->everyFiveMinutes()->withoutOverlapping();
        $schedule->command('dcci:bankinfo')->everyMinute()->withoutOverlapping();
        $schedule->command('dcci:payconfirm')->everyFiveMinutes()->withoutOverlapping();
        $schedule->command('dcci:status')->everyFiveMinutes()->withoutOverlapping();
        $schedule->command('dcci:shortfall')->everyFiveMinutes()->withoutOverlapping();
        $schedule->command('dcci:certificate')->everyFiveMinutes()->withoutOverlapping();
        $schedule->command('dcci:certificatezip')->everyFiveMinutes()->withoutOverlapping();

        










    }

    protected function commands()
    {
        require base_path('routes/console.php');
    }
}

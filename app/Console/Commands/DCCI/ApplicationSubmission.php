<?php


namespace App\Console\Commands\DCCI;


use App\Libraries\MonitorLog;
use App\Model\DCCI\RequestQueueDCCICos;
use App\Model\DCCI\DCCICosApps;
use App\Model\DCCI\DCCIPaymentConfirm;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

class ApplicationSubmission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dcci:formsubmission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'dcci form submission';

    /**
     * Create a new command instance.
     *
     */

    /*
     * File full path
     */

    private $file_path = '';
    private $DcciCommonFunction;

    public function __construct()
    {
        $this->DcciCommonFunction = new DcciCommonFunction();
        parent::__construct();
        $this->file_path = dirname(__FILE__) . "/" . basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
    }


    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {
        MonitorLog::cronAuditSave($this->file_path, 0, 0, 'DCCI COS form-submission[BCC-5]');

        $request_datas = RequestQueueDCCICos::orderBy('processing_at', 'asc')
            ->where('status', 0)
            ->limit(1)->get();

        if (count($request_datas) > 0) {
            $access_token = $this->DcciCommonFunction->getToken();
            if ($access_token != '') {
            	$file_root_path =  config('constant.dcci_local_file_path');
                foreach ($request_datas as $req) {
                    $url = config('constant.dcci_api_url') . 'submission';
                    $headers = array(
                        'Content-Type: multipart/form-data',
                        'Authorization: Bearer ' . $access_token,
                        'user-email:'.$req->applicant_mail,
                    );
                    $requestJsonDecoded = json_decode($req->request_json);

                    $postdata = array();
                    foreach($requestJsonDecoded as $key=>$value){
                    	if($key != 'application_dto'){
                    		$file_path = $value;
		                    if($file_path  ==null || $file_path  ==''){
		                        continue;
		                    }

		                    if($file_path != ''){
		                        $file_path_real_path = $file_root_path."/".$file_path ;
		                    }
		                   

		                    $filePath = str_replace("\\",'/', $file_path_real_path);

	                        if (!file_exists($filePath)){
	                            continue;
	                        }

	                        if (function_exists('curl_file_create')) {
                                $filePath = curl_file_create($filePath);
                            } else {
                                $filePath = '@' . realpath($filePath);
                                curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
                            }
                            
                            $postdata[$key] = $filePath;

                    	}
                    }
                    $postdata['application_dto'] =json_encode($requestJsonDecoded->application_dto);
                    $requestSend = Carbon::now()->format('Y-m-d H:i:s.u');
                    $responses = $this->DcciCommonFunction->curlPostRequest($url, $headers, $postdata);
                    $responseget = Carbon::now()->format('Y-m-d H:i:s.u');
                    $req->request_time = $requestSend;
                    $req->response_time = $responseget;
                    $req->processing_at = Carbon::now();
                    $req->no_of_try = $req->no_of_try+1;
                    $jsonResponseData = $responses['data'];
                    $req->response_json = $responses['data'];
                    if ($responses['http_code'] == 200) {
                        $responseData = json_decode($jsonResponseData);
                        $appType=($req->type== 'submission')?1:2;
                        if (isset($responseData->data->success) && $responseData->data->success  == true) {
                            if($req->type == 'submission'){
                                $this->applicationStatusEntry($req->ref_id, $jsonResponseData, 1);
                                $this->formStatusUpdate($req->id, $jsonResponseData, 1,5);
                            }elseif($req->type == 'resubmission'){
                                $this->formStatusUpdate($req->id, $jsonResponseData, 2,1);
                            }

                        } elseif(isset($responseData->data->success) && $responseData->data->success == false) {;
                            $this->formStatusUpdate($req->id, $jsonResponseData,$appType, -1);
                        } else {
                            $this->formStatusUpdate($req->id, $jsonResponseData,$appType, -2);
                        }
                    } else {
                        $this->formStatusUpdate($req->id, $responses['data'],$appType,-3);
                    }
                }
            }
        } else {
            echo "No data to submit";
        }
    }


    /**
     * @param $id
     * @param $jsonResponseData
     * @param $status
     */
    private function formStatusUpdate($id, $jsonResponseData,$type, $status)
    {
        if($type == 1){
            RequestQueueDCCICos::findOrFail($id)->update([
                'response_json' => $jsonResponseData,
                'status' => $status,
            ]);
        }else{
            RequestQueueDCCICos::findOrFail($id)->update([
                'response_json' => $jsonResponseData,
                'status' => $status,
                'origin_certificate_status'=> 0
            ]);
        }

    }

    /**
     * @param $ref_id
     * @param $jsonResponseData
     * @param $status
     */
    private function applicationStatusEntry($ref_id, $jsonResponseData, $status)
    {
        $responseData = json_decode($jsonResponseData);
        $dcci_tracking_no = $responseData->data->payload->trackingID;
//        update tracking no in Dcci cos apps table
        $application = DCCICosApps::find($ref_id);
        $application->dcci_cos_tracking_no = $dcci_tracking_no;
        $application->save();

        $paymentInfo = new DCCIPaymentConfirm();
        $paymentInfo->ref_id = $ref_id;
        $paymentInfo->app_fee_status = 0;
        $paymentInfo->app_account_status = 0;
        $paymentInfo->status = 0;
        $paymentInfo->save();

        }
}
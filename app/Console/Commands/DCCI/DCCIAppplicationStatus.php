<?php


namespace App\Console\Commands\DCCI;


use App\Libraries\MonitorLog;
use App\Model\DCCI\DCCICosApps;
use App\Model\DCCI\DCCICosShortfall;
use App\Model\DCCI\DCCICosStatusQueue;
use App\Model\DCCI\DCCICosStatusMapping;
use App\Model\DCCI\RequestQueueDCCICos;
use App\Model\DCCI\DCCIPaymentConfirm;
use App\ProcessList;

use Carbon\Carbon;
use Illuminate\Console\Command;

class DCCIAppplicationStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dcci:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'dcci application status';

    /**
     * Create a new command instance.
     *
     */

    /*
     * File full path
     */

    private $file_path = '';
    private $DcciCommonFunction;

    public function __construct()
    {
        $this->DcciCommonFunction = new DcciCommonFunction();
        parent::__construct();
        $this->file_path = dirname(__FILE__) . "/" . basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
    }


    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {
        MonitorLog::cronAuditSave($this->file_path, 0, 0, 'DCCI COS bank info[DCCI COS-5]');

        $request_datas = DCCICosStatusQueue::orderBy('processing_at', 'asc')
            ->where('status','!=','1')
            ->where('completed',0)
            ->limit(1)->get();
        if (count($request_datas) > 0) {

            $access_token = $this->DcciCommonFunction->getToken();
            if ($access_token != '') {
                foreach ($request_datas as $req) {
                    $user_info = RequestQueueDCCICos::where('ref_id', $req->ref_id)->first(['applicant_mail']);
                    $tracking_no = DCCICosApps::where('id', $req->ref_id)->first(['dcci_cos_tracking_no']);
                    $url = config('constant.dcci_api_url') . 'info/application-status/' . $tracking_no->dcci_cos_tracking_no;
                    $requestSend = Carbon::now()->format('Y-m-d H:i:s.u');
                    $responses = $this->DcciCommonFunction->curlGetRequest($url, $user_info->applicant_mail, $access_token);
                    $responseget = Carbon::now()->format('Y-m-d H:i:s.u');
                    $req->request_time = $requestSend;
                    $req->response_time = $responseget;
                    $req->no_of_try = $req->no_of_try+1;
                    $req->processing_at = Carbon::now();
                    $jsonResData = json_decode($responses['data']);
                    if ($responses['http_code'] == 200) {
                        $req->response = json_encode($jsonResData->data);
                        if($req->dcci_cos_status_code != $jsonResData->data->code){
                            $req->dcci_cos_status_code = $jsonResData->data->code;
                            $this->updateProcessList($jsonResData, $req->ref_id);
                        }
                    } else {
                        $req->response = $responses;
                        $req->status = -1;
                    }
                    $req->save();
                }
            }
        } else {
            echo "No data to submit";
        }
    }

    public function updateProcessList($decoded_response, $id)
    {
        $dcci_code = $decoded_response->data->code;
        if (isset($dcci_code)) {
            if($dcci_code == 'CO-0004'){
                $dcci_cos_shortfall = DCCICosShortfall::firstOrNew([
                    'ref_id'=>$id,
                ]);
                $dcci_cos_shortfall->status = 0;
                $dcci_cos_shortfall->save();
            }

            if($dcci_code == 'CO-0002'){
                RequestQueueDCCICos::where('ref_id',$id)->where('origin_certificate_status','10')->update([
                    'origin_certificate_status'=>'0'
                ]);
            }
            if($dcci_code == 'CO-0003'){
                RequestQueueDCCICos::where('ref_id',$id)->where('cosignor_certificate_status','10')->update([
                    'cosignor_certificate_status'=>'0'
                ]);
                DCCICosStatusQueue::where('ref_id',$id)->update([
                    'status'=>'1'
                ]);
            }

            $dcciData = DCCICosApps::where('id', $id)->first();
            if (isset($dcciData)) {
                $bidastatus = DCCICosStatusMapping::where('dcci_cos_status_id', $dcci_code)->first();
                $processdata = ProcessList::where('ref_id', $dcciData->id)->where('process_type_id', 123)->first();
                $processdata->status_id = $bidastatus->bida_status_id;
                $processdata->save();
                if(in_array( $bidastatus->bida_status_id,config('constant.complete_status_dcci'))){
                    DCCICosStatusQueue::where('ref_id',$bidastatus->id)->update([
                        'completed' => 1,
                    ]);
                }
            }

        }

    }
}
<?php


namespace App\Console\Commands\DCCI;


use App\Libraries\MonitorLog;
use App\Model\DCCI\RequestQueueDCCICos;
use App\Model\DCCI\DCCIPaymentConfirm;

use Carbon\Carbon;
use Illuminate\Console\Command;

class DCCIBankInfo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dcci:bankinfo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'dcci form submission';

    /**
     * Create a new command instance.
     *
     */

    /*
     * File full path
     */

    private $file_path = '';
    private $DcciCommonFunction;

    public function __construct()
    {
        $this->DcciCommonFunction = new DcciCommonFunction();
        parent::__construct();
        $this->file_path = dirname(__FILE__) . "/" . basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
    }


    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {
        MonitorLog::cronAuditSave($this->file_path, 0, 0, 'DCCI COS bank info[DCCI COS-5]');

        $request_datas = RequestQueueDCCICos::orderBy('processing_at', 'asc')
            ->where('status',5)
            ->limit(1)->get();

        if (count($request_datas) > 0) {

            $access_token = $this->DcciCommonFunction->getToken();
            if ($access_token != '') {
                foreach ($request_datas as $req) {
                    $user_info = RequestQueueDCCICos::where('ref_id', $req->ref_id)->first(['applicant_mail']);
                    $url = config('constant.dcci_api_url') . 'info/bank-info';

                    $requestSend = Carbon::now()->format('Y-m-d H:i:s.u');
                    $responses = $this->DcciCommonFunction->curlGetRequest($url, $user_info->applicant_mail, $access_token);
                    $responseget = Carbon::now()->format('Y-m-d H:i:s.u');

                    $req->request_time = $requestSend;
                    $req->response_time = $responseget;
                    $req->processing_at = Carbon::now();
                    $req->no_of_try = $req->no_of_try+1;
                    $jsonResData = json_decode($responses['data']);
                    $paymentInfo = DCCIPaymentConfirm::where('ref_id',$req->ref_id)->first();
                    if ($responses['http_code'] == 200) {
                        $paymentInfo->app_fee_account_json = json_encode($jsonResData->data);
                        $paymentInfo->app_account_status = 1;
                        $this->formStatusUpdate($req->id,1);
                    }else{
                        $paymentInfo->app_fee_account_json = json_encode($responses);
                        $paymentInfo->app_account_status = -1;
                        $this->formStatusUpdate($req->id,-2);
                    }
                    $paymentInfo->save();
                }
            }
        } else {
            echo "No data to submit";
        }
    }
    /**
     * @param $id
     * @param $jsonResponseData
     * @param $status
     */
    private function formStatusUpdate($id, $status)
    {
        RequestQueueDCCICos::findOrFail($id)->update([
            'status' => $status,
        ]);
    }

}
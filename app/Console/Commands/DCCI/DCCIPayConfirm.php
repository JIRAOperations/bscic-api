<?php
namespace App\Console\Commands\DCCI;

use App\Libraries\MonitorLog;
use App\Model\DCCI\DCCICosApps;
use App\Model\DCCI\DCCICosStatusQueue;
use App\Model\DCCI\DCCIPaymentConfirm;
use App\Model\DCCI\RequestQueueDCCICos;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;

class DCCIPayConfirm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dcci:payconfirm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'dcci payment confirm';

    /*
     * File full path
     */

    private $file_path = '';
    private $DcciCommonFunction;

    public function __construct()
    {
        $this->DcciCommonFunction = new DcciCommonFunction();
        parent::__construct();
        $this->file_path = dirname(__FILE__) . "/" . basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
    }

    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {
        MonitorLog::cronAuditSave($this->file_path, 0, 0, 'DCCI COS payment confirm[DCCICOS-5]');

        $request_datas = DCCIPaymentConfirm::orderBy('processing_at', 'asc')
            ->where('status', 0)
            ->limit(1)->get();

        if (count($request_datas) > 0) {
            $access_token = $this->DcciCommonFunction->getToken();
            if ($access_token != '') {
                foreach ($request_datas as $req) {
                    $user_info = RequestQueueDCCICos::where('ref_id', $req->ref_id)->first(['applicant_mail']);
                    $app_info = DCCICosApps::where('id', $req->ref_id)->first(['dcci_cos_tracking_no']);
                    $url = config('constant.dcci_api_url') . 'payment/'.$app_info->dcci_cos_tracking_no;
                    $headers = array(
                        'Content-Type: application/json',
                        'Authorization: Bearer ' . $access_token,
                        'user-email:'.$user_info->applicant_mail,
                    );

                    $requestSend = Carbon::now()->format('Y-m-d H:i:s.u');
                    $postData = $req->request_payment;
                    $responses = $this->DcciCommonFunction->curlPostRequest($url, $headers, $postData);
                    $responseget = Carbon::now()->format('Y-m-d H:i:s.u');
                    $req->request_time = $requestSend;
                    $req->response_time = $responseget;
                    $req->processing_at = Carbon::now();
                    $req->no_of_try = $req->no_of_try+1;
                    $jsonResData = $responses['data'];
                    if ($responses['http_code'] == 200) {
                        $req->response_payment = $jsonResData;
                        $req->status = 1;
                        $application_status_entry = new DCCICosStatusQueue();
                        $application_status_entry->ref_id = $req->ref_id;
                        $application_status_entry->status = 0;
                        $application_status_entry->save();
                    }else{
                        $req->response_payment = $responses;
                        $req->status = -1;
                    }
                    $req->save();
                }
            }
        } else {
            echo "No data to submit";
        }
    }
}
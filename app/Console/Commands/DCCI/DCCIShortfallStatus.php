<?php


namespace App\Console\Commands\DCCI;


use App\Libraries\MonitorLog;
use App\Model\DCCI\DCCICosApps;
use App\Model\DCCI\DCCICosShortfall;
use App\Model\DCCI\DCCICosStatusQueue;
use App\Model\DCCI\RequestQueueDCCICos;
use App\Model\DCCI\DCCIPaymentConfirm;

use Carbon\Carbon;
use Illuminate\Console\Command;

class DCCIShortfallStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dcci:shortfall';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'dcci application shortfall info';

    /**
     * Create a new command instance.
     *
     */

    /*
     * File full path
     */

    private $file_path = '';
    private $DcciCommonFunction;

    public function __construct()
    {
        $this->DcciCommonFunction = new DcciCommonFunction();
        parent::__construct();
        $this->file_path = dirname(__FILE__) . "/" . basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
    }


    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {
        MonitorLog::cronAuditSave($this->file_path, 0, 0, 'DCCI COS shortfall[DCCI COS-5]');

        $request_datas = DCCICosShortfall::orderBy('processing_at', 'asc')
            ->where('status', 0)
            ->limit(1)->get();

        if (count($request_datas) > 0) {

            $access_token = $this->DcciCommonFunction->getToken();
            if ($access_token != '') {
                foreach ($request_datas as $req) {
                    $user_info = RequestQueueDCCICos::where('ref_id', $req->ref_id)->first(['applicant_mail']);
                    $tracking_no = DCCICosApps::where('id', $req->ref_id)->first(['dcci_cos_tracking_no']);
                    $url = config('constant.dcci_api_url') . 'info/application-shortfall-status/' . $tracking_no->dcci_cos_tracking_no;
                    $requestSend = Carbon::now()->format('Y-m-d H:i:s.u');
                    $responses = $this->DcciCommonFunction->curlGetRequest($url, $user_info->applicant_mail, $access_token);
                    $responseget = Carbon::now()->format('Y-m-d H:i:s.u');
                    $req->request_time = $requestSend;
                    $req->response_time = $responseget;
                    $req->processing_at = Carbon::now();
                    $req->no_of_try = $req->no_of_try+1;
                    $jsonResData = json_decode($responses['data']);
                    if ($responses['http_code'] == 200) {
                        $req->response_json = json_encode($jsonResData->data);
                        $req->status = 1;
                    } else {
                        $req->response_json = $responses;
                        $req->status = -2;
                    }
                    $req->save();
                }
            }
        } else {
            echo "No data to submit";
        }
    }

}
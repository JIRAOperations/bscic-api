<?php


namespace App\Console\Commands\DCCI;


use App\Model\ApiTokenList;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class DcciCommonFunction
{
    // Get token for authorization
    public function getToken()
    {
        $processTypeId = 123;
        $tokenInfo =  ApiTokenList::where('process_type_id',$processTypeId)->first();
        if ($tokenInfo !=null) {
            $stored_token = $tokenInfo->token;
            $exp_time = strtotime($tokenInfo->valid_till);
            $current_time = strtotime(Carbon::now());

            if($exp_time > $current_time){
                return $stored_token;
            }
        }
        // Get credentials from env
        $idp_url = config('constant.bscic_idp_url');
        $client_id = config('constant.bscic_client_id');
        $client_secret = config('constant.bscic_client_secret');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 15);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'grant_type' => 'client_credentials'
        )));
        curl_setopt($curl, CURLOPT_URL, "$idp_url");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $result = curl_exec($curl);
        if (!$result) {
            $data = ['responseCode' => 0, 'msg' => 'API connection failed!'];
            return response()->json($data);
        }
        curl_close($curl);
        $decoded_json = json_decode($result, true);
        $token = $decoded_json['access_token'];
        $expired_time = config('constant.token_expired_time');
        $tokenInfo = ApiTokenList::firstOrNew(['process_type_id' => $processTypeId]);
        $tokenInfo->token = $token;
        $tokenInfo->valid_till = Carbon::now()->addMinute($expired_time);
        $tokenInfo->save();
        return $token;
    }

    /**
     * @param $url
     * @param $headers
     * @param $postdata
     * @return array
     */
    public function curlPostRequest($url, $headers, $postdata)
    {
        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
            $result = curl_exec($ch);

            if (!curl_errno($ch)) {
                $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            } else {
                $http_code = 0;
            }
            curl_close($ch);
            return ['http_code' => intval($http_code), 'data' => $result];
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param $requested_url
     * @param $token
     * @return array
     */
    public  function curlGetRequest($requested_url,$mail, $token)
    {
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $requested_url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => [
                    "authorization: Bearer " . $token,
                    'user-email:'.$mail,
                ],
            ));
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
            curl_setopt($curl, CURLOPT_TIMEOUT, 30);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);

            $curlResult = curl_exec($curl);
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            if (curl_errno($curl)) {
                $curlError = curl_error($curl);
                Log::info($curlError);
                $curlResult = null;
                echo $curlError;
            }
            curl_close($curl);
            return ['http_code' => intval($code), 'data' => $curlResult];
        } catch (\Exception $e) {
            Log::error($e->getMessage() .'. File : ' . $e->getFile() . ' [Line : ' . $e->getLine() . ']');
        }
    }

    /**
     * @param $url
     * @param $headers
     * @return array
     */
    public function curlGetResponse($url, $headers)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $result = curl_exec($ch);

        if (!curl_errno($ch)) {
            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        } else {
            $http_code = 0;
        }
        curl_close($ch);
        return ['http_code' => intval($http_code), 'data' => $result];

    }
}
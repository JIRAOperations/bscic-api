<?php


namespace App\Console\Commands\DOE;
use App\Libraries\MonitorLog;
use App\Model\DOE\DoeMaster;
use App\Model\DOE\DoeFileRequest;
use Illuminate\Console\Command;

class DoeShortfallDocSending extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doe:shortfalldoc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Doe shortfall Doc';

    /*
    * File full path
    */
    private $file_path = '';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->file_path = dirname(__FILE__)."/".basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
    }



    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle(){

        MonitorLog::cronAuditSave($this->file_path, 0, 0, 'doe-shortfalldoc-upload[DOE-4.1]');

        $docData = DoeFileRequest::where('status', 0)
        ->where('is_shortfall', 1)
            ->take(5)
            ->orderBy('created_at', 'asc')
            ->get(['id', 'status', 'ref_id','type','updated_at','file_path','submission_url']);
          

        if(count($docData) > 0){

           
                #MonitorLog::cronAuditSave($this->file_path, 0, 0, 'nr-doc-upload-submitted[NRR-2]');

                foreach ($docData as $req){

                    $appData = DoeMaster::find($req->ref_id);   
                    if($appData->reg_id == 0){                        
                        continue;
                    }


                    $status = DoeFileRequest::where('id', $req->id)
                        ->where('type', $req->type)
                        ->where('updated_at', $req->updated_at)
                        ->update(
                            [
                                'processing_at' => date('Y-m-d H:i:s', time()),
                                'response' => '',
                                'status' => -1
                            ]);
                    if(!$status){
                        continue;
                    }              

                                
$url =  $req->submission_url;

$filePath = str_replace("\\",'/', $req->file_path);
echo $filePath;
var_dump(file_exists($req->file_path));
if (!file_exists($req->file_path)){
    $this->docConfirm($req->id,'File not exist in project',-3);
    continue;
}
$ch = curl_init();


$forSavePostFields = array(
    'url' => $url,
    "$req->type" => $filePath,
    'user_id' => $appData->userId,
    'session_id' => $appData->sessionId,
    'id' =>  $appData->reg_id,
    'from_users_id' => $appData->shortfall_from_users_id,
    'comment' =>   $appData->shortfall_comment_from_user,
    'to_users_id' =>$appData->shortfall_to_users_id
);

$req->request_info = json_encode($forSavePostFields);
$req->save();




//If the function curl_file_create exists
if (function_exists('curl_file_create')) {
    $filePath = curl_file_create($filePath);
} else {
    $filePath = '@' . realpath($filePath);
    curl_setopt($ch, CURLOPT_SAFE_UPLOAD, false);
}
$postFields = array(
    "$req->type" => $filePath,
    'user_id' => $appData->userId,
    'session_id' => $appData->sessionId,
    'id' =>  $appData->reg_id,
    'from_users_id' => $appData->shortfall_from_users_id,
    'comment' =>   $appData->shortfall_comment_from_user,
    'to_users_id' =>$appData->shortfall_to_users_id
);
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
$response  = curl_exec($ch);
if (curl_errno($ch)) {
    echo 'Request Error:' . curl_error($ch);
}


                      
$message = $response;
$response = json_decode($response);



                        if(isset($response->status) && $response->status == true){                          
                                $this->docConfirm($req->id,$message,1);                           
                        }else{
                            $this->docConfirm($req->id,$message,-2);
                        }
                      
                }

                #MonitorLog::cronAuditSave($this->file_path, 0, 0, 'nr-doc-upload-response[NRR-2]');
            
        }
    }


    private function docConfirm($id,$msg,$status){
        DoeFileRequest::where('id', $id)
            ->update(
                [
                    'response' => $msg,
                    'status' => $status
                ]);
    }

   
}
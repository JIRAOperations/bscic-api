<?php
/**
 * User: root
 * Date: 28/06/2020
 * Time: 3:47 PM
 */

namespace App\Console\Commands\DOE;

use App\Libraries\MonitorLog;
use App\Model\DOE\DoeMaster;
use App\Model\DOE\DoeFileRequest;
use App\Model\DOE\DoeApplicationStatus;
use App\Model\DOE\DoeApiRequest;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Process\Process;


class DoeFormFinalSubmission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doe:final';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'DoE final submission';

    /**
     * Create a new command instance.
     *
     */

    /*
     * File full path
     */
    private $file_path = '';

    public function __construct()
    {
        parent::__construct();
        $this->file_path = dirname(__FILE__)."/".basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
    }


    public function curlPostRequest($url,$headers,$postdata)
    {
        try{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            // curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // curl_setopt($ch, CURLOPT_ENCODING, "");
            // curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
            // curl_setopt($ch, CURLOPT_TIMEOUT, 0);
            // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));


            $result = curl_exec($ch);

            if (!curl_errno($ch)) {
                $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            } else {
                $http_code = 0;
            }
            curl_close($ch);
            return ['http_code' => intval($http_code), 'data' => $result];

        }catch (\Exception $e){

            echo $e->getMessage();
        }
    }


       // Get token for authorization
       public function getToken(){
        
        // Get credentials from env
        $idp_url = config('constant.doe_idp_url');
        $client_id = config('constant.doe_client_id');
        $client_secret = config('constant.doe_client_secret');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'grant_type' => 'client_credentials'
        )));
        curl_setopt($curl, CURLOPT_URL, "$idp_url");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $result = curl_exec($curl);
        if(!$result){
            $data = ['responseCode' => 0, 'msg' => 'API connection failed!'];
            return response()->json($data);
        }
        curl_close($curl);
        $decoded_json = json_decode($result,true);
        $token = $decoded_json['access_token'];

        return $token;
    }

    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle(){

        try{
            MonitorLog::cronAuditSave($this->file_path, 0, 0, 'doe-final-submission[DOE-5]');


            $request_datas = DoeApiRequest::orderBy('final_processing_at', 'asc')
                ->where('status_form_2',1) 
                ->where('status_form_1',1)
                ->where('final_status', 0)                
                ->limit(5)->get();

            if(count($request_datas) > 0){

                $access_token = $this->getToken();

                if($access_token != ''){

                foreach ($request_datas as $req) {


                    $appData = DoeMaster::find($req->ref_id);

                    $url = config('constant.doe_api_url') . 'submission/final/'.$appData->transId;



                    $status = DoeApiRequest::where('id', $req->id)
                        ->where('final_status', $req->final_status)
                        ->where('final_processing_at', $req->final_processing_at)
                        ->update(
                            [          
                                'final_processing_at' => date('Y-m-d H:i:s', time()),  
                                'final_response' => "",    
                                'request_info3' =>  $url,                             
                                'final_status' => -1
                            ]);
                    if (!$status) {
                        continue;
                    }


                    $appData = DoeMaster::find($req->ref_id);

                    $url = config('constant.doe_api_url') . 'submission/final/'.$appData->transId;

                    $headers = array(
                        'Content-Type: application/json',
                        'Authorization: Bearer '.$access_token,
                    );

                    $postdata = '';

                    $response = $this->curlPostRequest($url,$headers,$postdata);                  

                    if($response['http_code'] == 200){

                        $message = $response['data'];
                        $response = json_decode($response['data']);
                        if(isset($response->responseCode) && $response->responseCode == '200'){
                            $this->formFinalStatusUpdate($req->id,$message,  1);
                            $this->applicationStatusEntry($req->ref_id,$message,  1);
                        }else{
                            $this->formFinalStatusUpdate($req->id,$message,  -2);
                        }
                    }else{
                        $this->formFinalStatusUpdate($req->id,$response['data'], -3);
                    }
                      
                }
            }
            }else{
                echo "No data to submit";
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo $e->getLine();
            echo "Something went wrong";
        }
    }
    private function formFinalStatusUpdate($id,$msg,  $status){
        DoeApiRequest::where('id', $id)
            ->update(
                [
                    'final_response' => $msg,
                    'final_status' => $status
                ]);
    }


    private function applicationStatusEntry($id,$msg,  $status){
        // DoeApplicationStatus::create(
        //         [
        //             'ref_id' => $id,
        //             'status' => 0
        //         ]);


                $DoeApplicationStatus = DoeApplicationStatus::firstOrNew(['ref_id' =>  $id]);
                $DoeApplicationStatus->ref_id =  $id;
                $DoeApplicationStatus->status = 0;
                $DoeApplicationStatus->request ='';
                $DoeApplicationStatus->response = '';
                $DoeApplicationStatus->doe_status = '';
                $DoeApplicationStatus->completed = 0;                
                $DoeApplicationStatus->processing_at = '';
                $DoeApplicationStatus->created_at = date('Y-m-d H:i:s',time());
                $DoeApplicationStatus->save();
    }
  
}
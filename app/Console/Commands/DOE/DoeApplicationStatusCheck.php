<?php
/**
 * User: root
 * Date: 28/06/2020
 * Time: 3:47 PM
 */

namespace App\Console\Commands\DOE;

use App\Libraries\MonitorLog;
use App\Model\DOE\DoeMaster;
use App\Model\DOE\DoeFileRequest;
use App\Model\DOE\Doecomment;
use App\Model\DOE\DoeChangeInfo;
use App\Model\DOE\DoeApplicationStatus;
use App\Model\DOE\DoeApiRequest;
use App\Model\DOE\DoeStatusMapping;
use App\ProcessList;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Process\Process;


class DoeApplicationStatusCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doe:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'DoE application status check';

    /**
     * Create a new command instance.
     *
     */

    /*
     * File full path
     */
    private $file_path = '';

    public function __construct()
    {
        parent::__construct();
        $this->file_path = dirname(__FILE__)."/".basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
    }


    public function curlGetResponse($url,$headers)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 150);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $result = curl_exec($ch);

        if (!curl_errno($ch)) {
            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        } else {
            $http_code = 0;
        }
        curl_close($ch);
        return ['http_code' => intval($http_code), 'data' => $result];
       
    }


       // Get token for authorization
       public function getToken(){
        
        // Get credentials from env
        $idp_url = config('constant.doe_idp_url');
        $client_id = config('constant.doe_client_id');
        $client_secret = config('constant.doe_client_secret');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'grant_type' => 'client_credentials'
        )));
        curl_setopt($curl, CURLOPT_URL, "$idp_url");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $result = curl_exec($curl);
        if(!$result){
            $data = ['responseCode' => 0, 'msg' => 'API connection failed!'];
            return response()->json($data);
        }
        curl_close($curl);
        $decoded_json = json_decode($result,true);
        $token = $decoded_json['access_token'];

        return $token;
    }

    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle(){

        try{
            MonitorLog::cronAuditSave($this->file_path, 0, 0, 'doe-application-status[DOE-6]');


            $request_datas = DoeApplicationStatus::orderBy('updated_at', 'asc')
                ->where('completed', 0) 
                ->where('status', '!=', -1)                
                ->limit(5)->get();

            if(count($request_datas) > 0){

                $access_token = $this->getToken();

                if($access_token != ''){

                foreach ($request_datas as $req) {

                    $status = DoeApplicationStatus::where('id', $req->id)
                        ->where('completed', $req->completed)
                        ->where('updated_at', $req->updated_at)
                        ->update(
                            [          
                                'processing_at' => date('Y-m-d H:i:s', time()),                      
                                'response' => '',
                                'doe_status' => '',
                                'status' => -1
                            ]);
                    if (!$status) {
                        continue;
                    }


                    $appData = DoeMaster::find($req->ref_id);

                    $url = config('constant.doe_api_url') . 'application/status/'.$appData->transId;
                    

                    $headers = array(
                        'Content-Type: application/json',
                        'Authorization: Bearer '.$access_token,
                    );


                   

                    $response = $this->curlGetResponse($url,$headers);                  
                    $req->request =  $url;
                    $req->response = $response['data'];
                    $req->save();


                    if($response['http_code'] == 200){

                        $message = $response['data'];

                        
                        $response = json_decode($response['data']);
                        if(isset($response->responseCode) && $response->responseCode == '200'){
                            $this->applicationStatusUpdate($req->id, $req->ref_id, $message,  1);
                            // $this->applicationStatusEntry($req->ref_id,$message,  1);
                        }else{
                            $this->applicationStatusUpdate($req->id, $req->ref_id, $message,  -2);
                        }
                    }else{
                        $this->applicationStatusUpdate($req->id, $req->ref_id, $response['data'], -3);
                    }
                      
                }
            }
            }else{
                echo "No data to submit";
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo $e->getLine();
            echo "Something went wrong";
        }
    }
    private function applicationStatusUpdate($id, $ref_id, $msg,  $status){
echo "$ref_id";
        $doe_status = '';
         $doe_status_global='';

        if($status == 1){
            
             $doe_status = json_decode($msg, true);
              $doe_status_main = json_decode($msg, true);
            $doe_status = $doe_status['data'][0]['status'];
            $doe_status_global = $doe_status_main['data'][0]['status'];
            

            $bezastatus = DoeStatusMapping::where('doe_status', $doe_status)->first();
           // if both status same process list need not be updated
           $checkPrListStatus = ProcessList::where('process_type_id',108)->where('ref_id',$ref_id)->first();
           if($bezastatus->status_id != $checkPrListStatus->status_id){
               ProcessList::where('process_type_id',108)->where('ref_id',$ref_id)->update(
                   [
                       'status_id' => $bezastatus->status_id,
                       'desk_id' => 0
                   ]);
           }

        }
        
        DoeApplicationStatus::where('id', $id)
            ->update(
                [
                    'response' => $msg,
                    'doe_status' => $doe_status,
                    'status' => $status
                ]);


       
            

            // $doe_status = json_decode($msg, true);
             $doe_status = json_decode($msg, true);
              $doe_status = $doe_status['data'][0];

                 DoeMaster::where('id',$ref_id)->update(
        [
            'file_no' => $doe_status['file_no']           
        ]);

        $doeMasterData = DoeMaster::where('id',$ref_id)->first();

if(isset($doe_status['comments'][0]['registration_id'])){ // for shortfall
    $all_comment =  $doe_status['comments'];
    $shortfall_reg_id = $doe_status['comments'][0]['registration_id'];
    $shortfall_from_users_id = $doe_status['comments'][0]['from_users_id'];            
    $shortfall_to_users_id = $doe_status['comments'][0]['to_users_id'];
    $last_comment_id = $doe_status['comments'][0]['id']; 

    $comment = $doe_status['comments'][0]['comment'];                        
    $shortfall_date_time = $doe_status['comments'][0]['date_time'];
    $sender_type = $doe_status['comments'][0]['sender_type'];

    
   

        if($doeMasterData->last_comment_id !=  $last_comment_id && $sender_type == 'admin'){
            DoeMaster::where('id',$ref_id)->update(
                [
                    'last_comment_id' => $last_comment_id,
                    'shortfall_reg_id' => $shortfall_to_users_id,
                    'shortfall_from_users_id' => $shortfall_to_users_id, 
                    'shortfall_to_users_id' => $shortfall_from_users_id,
                    'shortfall_date_time' => $shortfall_date_time
                ]);

            ProcessList::where('process_type_id',108)->where('ref_id',$ref_id)->update(
                [
                    'process_desc' => $comment,
                    'desk_id' => 0
                ]);

              
        }

        foreach($all_comment as $value){

            $comment_id = $value['id'];
            $comment =  $value['comment'];   
            $sender_type =   $value['sender_type'];
            $comment_date_time=  $value['date_time'];
            $comment_attachments= "";
            if(isset($value['comment_attachments'][0]['file_attach'])){
                $comment_attachments=  $value['comment_attachments'][0]['file_attach'];
            }
            
            
            $DoeCommentHistory = Doecomment::firstOrNew([
                'comment_id' =>   $comment_id,
                'ref_id' =>  $ref_id
                ]);

            $DoeCommentHistory->ref_id = $ref_id;
            $DoeCommentHistory->comment =  $comment;
            $DoeCommentHistory->comment_id =   $comment_id;

            $DoeCommentHistory->sender_type =   $sender_type;
            $DoeCommentHistory->comment_date_time = $comment_date_time;
            $DoeCommentHistory->attachment = $comment_attachments; 
            $DoeCommentHistory->full_json =  json_encode($value);

            $DoeCommentHistory->save();
        }
    

}

if(isset($doe_status['certificate'][0]['registration_id']) &&  $doe_status_global='completed'){ // for complete status
    $certificate = $doe_status['certificate'][0]['file_certificate'];
    $completed_at = $doe_status['certificate'][0]['date'];
    DoeMaster::where('id',$ref_id)->update(
        [
            'certificate' => $certificate           
        ]);

            DoeApplicationStatus::where('id', $id)
            ->update(
                [
                    'completed' => 1
                ]);

        ProcessList::where('process_type_id',108)->where('ref_id',$ref_id)->update(
            [
                'completed_at' => $completed_at
            ]);

}

if(isset($doe_status['changes'])){// for change entry
    
    $DoeChangeInfo = DoeChangeInfo::firstOrNew([
        'ref_id' =>  $ref_id
        ]);
    $DoeChangeInfo->ref_id = $ref_id;
    $DoeChangeInfo->change_info_json = json_encode($doe_status['changes']);    
    $DoeChangeInfo->save();

}
               
    }



  
}
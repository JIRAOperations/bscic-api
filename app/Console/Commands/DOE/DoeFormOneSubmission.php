<?php
/**
 * User: root
 * Date: 28/06/2020
 * Time: 3:47 PM
 */

namespace App\Console\Commands\DOE;

use App\Libraries\MonitorLog;
use App\Model\DOE\DoeMaster;
use App\Model\DOE\DoeFileRequest;
use App\Model\CDA\User;
use App\Model\DOE\DoeApiRequest;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Process\Process;


class DoeFormOneSubmission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doe:form1';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'DoE form 1';

    /**
     * Create a new command instance.
     *
     */

    /*
     * File full path
     */
    private $file_path = '';

    public function __construct()
    {
        parent::__construct();
        $this->file_path = dirname(__FILE__)."/".basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
    }


    public function curlPostRequest($url,$headers,$postdata)
    {
        try{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));
            $result = curl_exec($ch);

            if (!curl_errno($ch)) {
                $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            } else {
                $http_code = 0;
            }
            curl_close($ch);
            return ['http_code' => intval($http_code), 'data' => $result];

        }catch (\Exception $e){

            echo $e->getMessage();
        }
    }


       // Get token for authorization
       public function getToken(){
        
        // Get credentials from env
        $idp_url = config('constant.doe_idp_url');
        $client_id = config('constant.doe_client_id');
        $client_secret = config('constant.doe_client_secret');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'grant_type' => 'client_credentials'
        )));
        curl_setopt($curl, CURLOPT_URL, "$idp_url");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $result = curl_exec($curl);
        if(!$result){
            $data = ['responseCode' => 0, 'msg' => 'API connection failed!'];
            return response()->json($data);
        }
        curl_close($curl);
        $decoded_json = json_decode($result,true);
        $token = $decoded_json['access_token'];

        return $token;
    }

    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle(){

        try{
            MonitorLog::cronAuditSave($this->file_path, 0, 0, 'doe-form1-data[DOE-2]');


            $request_datas = DoeApiRequest::orderBy('form1_processing_at', 'asc')
                ->where('status_form_1',0)  
                ->where('status_form_2',0)  
                ->where('final_status',0)
                // ->where('doe_file',1)
                // ->where('doe_file2',1)                
                ->limit(5)->get();

            if(count($request_datas) > 0){

                $access_token = $this->getToken();

                if($access_token != ''){

                foreach ($request_datas as $req) {

                    $appData = DoeMaster::find($req->ref_id);

                    if($appData->transId == 0){
                        continue;
                       }

                    $url = config('constant.doe_api_url') . 'submission/form1/'.$appData->transId;

                    $status = DoeApiRequest::where('id', $req->id)
                        ->where('status_form_1', $req->status_form_1)
                        ->where('form1_processing_at', $req->form1_processing_at)
                        ->update(
                            [          
                                'form1_processing_at' => date('Y-m-d H:i:s', time()),
                                'response_form1' => "",   
                                'request_info1' =>  $url,                     
                                'status_form_1' => -1
                            ]);
                    if (!$status) {
                        continue;
                    }


                   

                    $doeFileRequest = DoeFileRequest::where('ref_id', $req->ref_id)->get();



                    $url = config('constant.doe_api_url') . 'submission/form1/'.$appData->transId;

                    $headers = array(
                        'Content-Type: application/json',
                        'Authorization: Bearer '.$access_token,
                    );

                    $postdata = json_decode($req->request_json_form_1,true);

                    $response = $this->curlPostRequest($url,$headers,$postdata);                  

                    if($response['http_code'] == 200){

                        $message = $response['data'];
                        $response = json_decode($response['data']);
                        if(isset($response->responseCode) && $response->responseCode == '200'){
                            $this->doeMasterRegIdUpdate($req->ref_id,$message,  1);
                            $this->formOneStatusUpdate($req->id,$message,  1);
                        }else{
                            $this->formOneStatusUpdate($req->id,$message,  -2);
                        }
                    }else{
                        $this->formOneStatusUpdate($req->id,$response['data'], -3);
                    }
                      
                }
            }
            }else{
                echo "No data to submit";
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo $e->getLine();
            echo "Something went wrong";
        }
    }
    private function formOneStatusUpdate($id,$msg,  $status){
        DoeApiRequest::where('id', $id)
            ->update(
                [
                    'response_form1' => $msg,
                    'status_form_1' => $status
                ]);

                
    }

    private function doeMasterRegIdUpdate($ref_id,$msg,  $status){       
                if($status == 1){
                    $reg_id = json_decode($msg, true);
                    $reg_id = $reg_id['data']['id'];                   
        
                    DoeMaster::where('id',$ref_id)->update(
                        [
                            'reg_id' => $reg_id
                        ]);
        
                }
    }
  
}
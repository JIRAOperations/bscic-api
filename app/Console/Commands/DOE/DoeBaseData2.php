<?php
/**
 * User: root
 * Date: 28/06/2020
 * Time: 3:47 PM
 */

namespace App\Console\Commands\DOE;

use App\Libraries\MonitorLog;
use App\Model\DOE\DoeMaster;
use App\Model\DOE\DoeFileRequest;
use App\Model\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Process\Process;


class DoeBaseData2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doe:basedata2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'DoE Base Data2';

    /**
     * Create a new command instance.
     *
     */

    /*
     * File full path
     */
    private $file_path = '';

    public function __construct()
    {
        parent::__construct();
        $this->file_path = dirname(__FILE__)."/".basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
    }


    public function curlGetResponse($url,$headers)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 150);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        $result = curl_exec($ch);

        if (!curl_errno($ch)) {
            $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        } else {
            $http_code = 0;
        }
        curl_close($ch);
        return ['http_code' => intval($http_code), 'data' => $result];
       
    }

    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle(){

        try{
            MonitorLog::cronAuditSave($this->file_path, 0, 0, 'doe-base-data2[DOE-1.1]');


            $request_datas = DoeMaster::orderBy('updated_at', 'asc')
                ->where('doe_file2',0)  
                ->where('doe_file',1)                
                ->limit(10)->get();

            if(count($request_datas) > 0){
                foreach ($request_datas as $request_data) {
                    $appData = DoeMaster::find($request_data->id);
                    // $request_data->certificate = $appData->certificate;
                    // $request_data->save();
                    
                        echo $request_data->id."</br>";
                        $token = $this->getToken();
                        if($token != ''){
                          
                            $url = config('constant.doe_api_url') . 'info/form-2-upload/'.$appData->transId;
                    

                            $headers = array(
                                'Content-Type: application/json',
                                'Authorization: Bearer '.$token,
                            );
        
        
                           
        
                            $baseDataResponse = $this->curlGetResponse($url,$headers);
                            $request_data->base2_api_response  =  $baseDataResponse['data'];
                            $request_data->save();


                            $baseDataResponse = json_decode($baseDataResponse['data'], true);
                            
                            
                             
$fileData = $baseDataResponse['data']['form2UploadDocs'];
$file_root_path =  config('constant.doe_file_root_path');
foreach($fileData as $row){
  
    echo $row."</br>";
    $file_path = $appData->$row;
    $doeFile =  DoeFileRequest::firstOrNew([
        'ref_id' => $request_data->id,
        'type' =>  $row
        ]);
        
 $doeFile->ref_id = $request_data->id;
 $doeFile->type = $row;
 if($file_path != ''){
    $doeFile->file_path = $file_root_path."/".$file_path ;
 } 
 $doeFile->submission_url = $baseDataResponse['data']['fileUploadLink'];
 $doeFile->response = "";
 $doeFile->request_info = "";
 $doeFile->status = 0;
 $doeFile->save();
}                             
                          
// dd($baseDataResponse->data->form1UploadDocs);


            }else{
                $request_data->doe_file2 = -1;
                $request_data->save();
            }

            $request_data->doe_file2 = 1;
            $request_data->save();     
                }
                   
            }else{
                echo "No data to submit";
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo $e->getLine();
            echo "Something went wrong";
        }
    }

    // Get token for authorization
    public function getToken(){
        
        // Get credentials from env
        $idp_url = config('constant.doe_idp_url');
        $client_id = config('constant.doe_client_id');
        $client_secret = config('constant.doe_client_secret');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'grant_type' => 'client_credentials'
        )));
        curl_setopt($curl, CURLOPT_URL, "$idp_url");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $result = curl_exec($curl);
        if(!$result){
            $data = ['responseCode' => 0, 'msg' => 'API connection failed!'];
            return response()->json($data);
        }
        curl_close($curl);
        $decoded_json = json_decode($result,true);
        $token = $decoded_json['access_token'];

        return $token;
    }

  

  
}
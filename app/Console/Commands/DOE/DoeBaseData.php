<?php
/**
 * User: root
 * Date: 28/06/2020
 * Time: 3:47 PM
 */

namespace App\Console\Commands\DOE;

use App\Libraries\MonitorLog;
use App\Model\DOE\DoeMaster;
use App\Model\DOE\DoeFileRequest;
use App\Model\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Process\Process;


class DoeBaseData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doe:basedata';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'DoE Base Data';

    /**
     * Create a new command instance.
     *
     */

    /*
     * File full path
     */
    private $file_path = '';

    public function __construct()
    {
        parent::__construct();
        $this->file_path = dirname(__FILE__)."/".basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
    }


    public function curlPostRequest($url,$headers,$postdata)
    {
        try{
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata));
            $result = curl_exec($ch);

            if (!curl_errno($ch)) {
                $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            } else {
                $http_code = 0;
            }
            curl_close($ch);
            return ['http_code' => intval($http_code), 'data' => $result];

        }catch (\Exception $e){

            echo $e->getMessage();
        }
    }

    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle(){

        try{
            MonitorLog::cronAuditSave($this->file_path, 0, 0, 'doe-base-data[DOE-1]');


            $request_datas = DoeMaster::orderBy('updated_at', 'asc')
                ->where('doe_file',0)    
                ->where('is_submit',1)                              
                ->limit(10)->get();

            if(count($request_datas) > 0){
                foreach ($request_datas as $request_data) {
                    $appData = DoeMaster::find($request_data->id);
                    
                        echo $request_data->id."</br>";
                        $token = $this->getToken();
                        if($token != ''){
                          
                            $baseDataResponse = $this->getUserId($token,$request_data->id, $appData);

            $fileData = $baseDataResponse->data->uploadDocNames;
            $file_root_path =  config('constant.doe_file_root_path');
            foreach($fileData as $row){
                // dd(row);
                echo $row."</br>";
                $file_path = $appData->$row;
                $doeFile =  DoeFileRequest::firstOrNew([
                    'ref_id' => $request_data->id,
                    'type' =>  $row
                    ]);

             $doeFile->ref_id = $request_data->id;
             $doeFile->type = $row;
             if($file_path != ''){
                $doeFile->file_path = $file_root_path."/".$file_path ;
             }
             $doeFile->submission_url = $baseDataResponse->data->fileUploadLink;
             $doeFile->response = "";
             $doeFile->request_info = "";
             $doeFile->status = 0;
             $doeFile->save();
            }
                          
// dd($baseDataResponse->data->form1UploadDocs);


            }else{
                $request_data->doe_file = -1;
                $request_data->save();
            }

            $request_data->doe_file = 1;
            $request_data->save();     
                }
                   
            }else{
                echo "No data to submit";
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo $e->getLine();
            echo "Something went wrong";
        }
    }

    // Get token for authorization
    public function getToken(){
        
        // Get credentials from env
        $idp_url = config('constant.doe_idp_url');
        $client_id = config('constant.doe_client_id');
        $client_secret = config('constant.doe_client_secret');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'grant_type' => 'client_credentials'
        )));
        curl_setopt($curl, CURLOPT_URL, "$idp_url");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $result = curl_exec($curl);
        if(!$result){
            $data = ['responseCode' => 0, 'msg' => 'API connection failed!'];
            return response()->json($data);
        }
        curl_close($curl);
        $decoded_json = json_decode($result,true);
        $token = $decoded_json['access_token'];

        return $token;
    }

    public function getUserId($access_token,$ref_id, $appData){
   
        $appData =  DoeMaster::where('id',$ref_id)->first();
        $userdata =  User::find($appData->created_by);
        if(!isset($userdata)){
            return 0;
        }
        $user_name = $userdata->user_full_name != null && $userdata->user_full_name != "" ? $userdata->user_full_name:$userdata->user_first_name. ' '.  $userdata->user_middle_name.' '.$userdata->user_last_name;



            $url = config('constant.doe_api_url') . 'info/base-data';

                        $headers = array(
                            'Content-Type: application/json',
                            'Authorization: Bearer '.$access_token,
                        );

                        $postdata = array(
                            'clientName' => config('constant.doe_user_client_id'),
                            'userEmail' => $userdata->user_email
                        );

                        $response = $this->curlPostRequest($url,$headers,$postdata);



                        if($response['http_code'] == 200){

                            $message = $response['data'];
                            $response = json_decode($response['data']);
                            if(isset($response->responseCode) && $response->responseCode == '200'){
                                $this->doeUserIdUpdate($ref_id,$message,  $appData);
                            }else{
                                $this->doeUserIdUpdate($ref_id,$message,  $appData);
                            }
                        }else{
                            $this->doeUserIdUpdate($ref_id,$response['data'],  $appData);
                        }



                        return $response;
        

    }

    private function doeUserIdUpdate($id,$msg,  $appData){
        
        $responseData = json_decode($msg);
        $transId =  $appData->transId;
        if(isset($responseData->data->transId) && $transId == 0){
           $transId = $responseData->data->transId;
        }
        $userId =  $appData->userId;
        if(isset($responseData->data->userId) && $userId == 0){
            $userId = $responseData->data->userId;
         }

         $sessionId =  $appData->sessionId;
        if(isset($responseData->data->sessionId) && $sessionId == 0){
            $sessionId = $responseData->data->sessionId;
         }

        DoeMaster::where('id', $id)
            ->update(
                [
                    'base_api_response' => $msg,
                    'transId' =>  $transId,                    
                    'userId' =>  $userId ,      
                    'sessionId' =>  $sessionId ,
                ]);
    }
}
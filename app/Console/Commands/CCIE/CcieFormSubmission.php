<?php

/**
 * User: root
 * Date: 28/06/2020
 * Time: 3:47 PM
 */

namespace App\Console\Commands\CCIE;

use App\Libraries\MonitorLog;
use App\Model\CCIE\RequestQueueCCIE;
use App\Model\CCIE\CcieApplicationStatus;
use App\Model\CCIE\CcieApps;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CcieFormSubmission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccie:formsubmission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'CCIE form submission';

    /**
     * Create a new command instance.
     *
     */

    /*
     * File full path
     */

    private $file_path = '';
    private $ccieCommonFunction;

    public function __construct()
    {

        parent::__construct();
        $this->file_path = dirname(__FILE__) . "/" . basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
        $this->ccieCommonFunction = new CcieCommonFunction();
    }


    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {
        try {
            MonitorLog::cronAuditSave($this->file_path, 0, 0, 'CCIE-submission[CCIE-5]');
            // DB::enableQueryLog();
            $request_datas = RequestQueueCCIE::orderBy('processing_at', 'asc')
                ->where('status', 0)
                ->limit(1)->get();

            if (count($request_datas) > 0) {
                // dd('ok');
                $access_token = $this->ccieCommonFunction->getToken();
                if ($access_token != '') {
                    foreach ($request_datas as $req) {
                        $url = config('constant.ccie_api_url') . 'submission';
                        $headers = array(
                            'Content-Type: application/json',
                            'Authorization: Bearer ' . $access_token,
                        );
                        $postdata = $req->request_json;
                        $requestSend = Carbon::now()->format('Y-m-d H:i:s.u');
                        $responses = $this->ccieCommonFunction->curlPostRequest($url, $headers, $postdata);
                        $responseget = Carbon::now()->format('Y-m-d H:i:s.u');

                        $req->request_time = $requestSend;
                        $req->response_time = $responseget;
                        $req->processing_at = Carbon::now();
                        $req->no_of_try = $req->no_of_try + 1;

                        $jsonResponseData = $responses['data'];
                        $req->response_json = $jsonResponseData;
                        if ($responses['http_code'] == 200) {

                            $responseData = json_decode($jsonResponseData);
                            if (isset($responseData->responseCode) && $responseData->responseCode == '200') {
                                $this->ccieAppsUpdate($req->ref_id, $jsonResponseData, 1);
                                $this->applicationStatusEntry($req->ref_id, $jsonResponseData, 1);
                                $status = 1;
                            } else {
                                $status = -2;
                            }
                        } else {
                            $status = -3;
                        }
                        $req->status = $status;
                        $req->save();
                    }
                }
            } else {
                echo "No data to submit";
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo $e->getLine();
            echo "Something went wrong";
        }
    }


    /**
     * @param $ref_id
     * @param $jsonResponseData
     * @param $status
     */
    private function applicationStatusEntry($ref_id, $jsonResponseData, $status)
    {

        $responseData = json_decode($jsonResponseData);

        $decoded_response = $responseData->data;
        $tracking_no = $decoded_response->tracking_number;

        $applicationStatus = CcieApplicationStatus::firstOrNew(['ref_id' => $ref_id]);
        $applicationStatus->ref_id = $ref_id;
        $applicationStatus->status = 0;
        $applicationStatus->request = '';
        $applicationStatus->response = '';
        $applicationStatus->ccie_tracking_no = $tracking_no;
        $applicationStatus->ccie_status = 0;
        $applicationStatus->completed = 0;
        $applicationStatus->processing_at = '';
        $applicationStatus->created_at = Carbon::now();
        $applicationStatus->save();
    }

    /**
     * @param $ref_id
     * @param $jsonResponseData
     * @param $status
     */
    private function ccieAppsUpdate($ref_id, $jsonResponseData, $status)
    {

        if ($status == 1) {
            $responseData = json_decode($jsonResponseData);
            $decoded_response = $responseData->data;
            $tracking_no = $decoded_response->tracking_number;
            $application = CcieApps::find($ref_id);
            $application->ccie_tracking_no = $tracking_no;
            $application->save();
        }


    }


}

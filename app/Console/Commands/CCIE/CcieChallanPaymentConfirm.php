<?php


namespace App\Console\Commands\CCIE;

use App\Libraries\MonitorLog;
use App\Model\CCIE\CcieApps;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Model\CCIE\CcieChallanConfirm;


class CcieChallanPaymentConfirm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccie:challanconfirm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'after payment data sending ';

    /**
     * Create a new command instance.
     *
     */

    /*
     * File full path
     */
    private $file_path = '';
    private $ccieCommonFunction;

    public function __construct()
    {
        parent::__construct();
        $this->file_path = dirname(__FILE__) . "/" . basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
        $this->ccieCommonFunction = new CcieCommonFunction();
    }


    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {

        try {
            MonitorLog::cronAuditSave($this->file_path, 0, 0, 'pay-confirm[CCIE-4]');


            $request_datas = CcieChallanConfirm::orderBy('processing_at', 'asc')
                ->where('status', 0)
                ->limit(1)->get();

            if (count($request_datas) > 0) {
                $access_token = $this->ccieCommonFunction->getToken();
                if ($access_token != '') {
                    foreach ($request_datas as $req) {
                        $appData = CcieApps::find($req->ref_id);
                        if (!$appData->ccie_tracking_no) {
                            continue;
                        }
                        $url = config('constant.ccie_api_url') . 'payment';
                        $headers = array(
                            'Content-Type: application/json',
                            'Authorization: Bearer ' . $access_token,
                        );
                        $postdata = $req->request_ccie;
                        $requestSend = Carbon::now()->format('Y-m-d H:i:s.u');
                        $response = $this->ccieCommonFunction->curlPostRequest($url, $headers, $postdata);
                        $responseget = Carbon::now()->format('Y-m-d H:i:s.u');

                        $req->response_ccie = $response['data'];
                        $req->request_time = $requestSend;
                        $req->response_time = $responseget;
                        $req->processing_at = Carbon::now();
                        $req->no_of_try = $req->no_of_try + 1;

                        if ($response['http_code'] == 200) {
                            $message = $response['data'];
                            $response = json_decode($response['data']);
                            if (isset($response->responseCode) && $response->responseCode == '200') {
                                $req->status = 1;
                            } else {
                                $req->status = -1;
                            }
                        } else {
                            $req->status = -3;
                        }

                        $req->save();

                    }
                }
            } else {
                echo "No data to submit";
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo $e->getLine();
            echo "Something went wrong";
        }
    }


}
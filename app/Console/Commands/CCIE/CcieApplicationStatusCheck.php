<?php
/**
 * User: root
 * Date: 28/06/2020
 * Time: 3:47 PM
 */

namespace App\Console\Commands\CCIE;

use App\Libraries\MonitorLog;
use App\Model\CCIE\CcieApplicationStatus;
use App\Model\CCIE\CcieApps;
use App\Model\CCIE\CCIEShortfall;
use App\Model\DOE\DoeFileRequest;
use App\Model\DOE\Doecomment;
use App\Model\DOE\DoeChangeInfo;
use App\Model\DOE\DoeApplicationStatus;
use App\Model\DOE\DoeApiRequest;
use App\Model\CCIE\CcieStatusMapping;
use App\ProcessList;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Process\Process;


class CcieApplicationStatusCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccie:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = ' application status check';

    /**
     * Create a new command instance.
     *
     */

    /*
     * File full path
     */
    private $file_path = '';

    public function __construct()
    {
        parent::__construct();
        $this->file_path = dirname(__FILE__) . "/" . basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
        $this->ccieCommonFunction = new CcieCommonFunction();
    }


    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle()
    {

        try {
            MonitorLog::cronAuditSave($this->file_path, 0, 0, 'ccie-application-status[CCIE-2]');


            $request_datas = CcieApplicationStatus::orderBy('processing_at', 'asc')
                ->where('completed', 0)
                ->where('status', '!=', -1)
                ->limit(1)->get();

            if (count($request_datas) > 0) {
                $access_token = $this->ccieCommonFunction->getToken();
                if ($access_token != '') {
                    foreach ($request_datas as $req) {
                        $appData = CcieApps::find($req->ref_id);
                        $url = config('constant.ccie_api_url') . 'info/application-status/' . $appData->ccie_tracking_no;
                        $headers = array(
                            'Content-Type: application/json',
                            'Authorization: Bearer ' . $access_token,
                        );

                        $requestSend = Carbon::now()->format('Y-m-d H:i:s.u');
                        $response = $this->ccieCommonFunction->curlGetResponse($url, $headers);
                        $responseget = Carbon::now()->format('Y-m-d H:i:s.u');
                        $req->request_time = $requestSend;
                        $req->response_time = $responseget;
                        $req->processing_at = Carbon::now();
                        $req->no_of_try = $req->no_of_try + 1;
                        $req->request = $url;
                        $req->response = $response['data'];
                        $req->save();

                        //dd($req);


                        if ($response['http_code'] == 200) {
                            $message = $response['data'];
                            $response = json_decode($response['data']);
                            $ccie_tracking_no = $req->ccie_tracking_no;
                            if (isset($response->responseCode) && $response->responseCode == '200') {
                                $this->applicationStatusUpdate($req->id, $req->ref_id, $message, 1, $ccie_tracking_no);
                            } else {
                                $this->applicationStatusUpdate($req->id, $req->ref_id, $message, -2);
                            }
                        } else {
                            $this->applicationStatusUpdate($req->id, $req->ref_id, $response['data'], -3);
                        }

                    }
                }
            } else {
                echo "No data to submit";
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo $e->getLine();
            echo "Something went wrong";
        }
    }

    private function applicationStatusUpdate($id, $ref_id, $msg, $status, $ccie_tracking_no = null)
    {
        $ccie_status = '';
        $ccie_status_global = '';

        if ($status == 1) {

            $ccie_status = json_decode($msg, true);
            $decodedMsg = json_decode($msg, true);
            $ccie_status = $ccie_status['data']['application_status'];
            $appStatus = CcieStatusMapping::where('ccie_status', $ccie_status)->first();
            // if both status same process list need not be updated
            $checkPrListStatus = ProcessList::where('process_type_id', 113)->where('ref_id', $ref_id)->first();
            if (isset($decodedMsg['data']['resubmit_status']) && $decodedMsg['data']['resubmit_status'] == 1) {
                $shortFall = CCIEShortfall::firstOrNew(['ref_id' => $ref_id]);
                $shortFall->status = 0;
                $shortFall->ccie_tracking_no = $ccie_tracking_no;
                $shortFall->save();

                CcieApps::where('id', $ref_id)
                    ->update(
                        [
                            'is_submit_shortfall' => 0
                        ]);

                // new code for feedback
            }

//           if($appStatus->status_id != $checkPrListStatus->status_id){
//               ProcessList::where('process_type_id',113)->where('ref_id',$ref_id)->update(
//                   [
//                       'status_id' => $appStatus->status_id,
//                       'desk_id' => 0
//                   ]);
//
//           }
            // new code for feedback
            if (isset($decodedMsg['data']['resubmit_status']) && $decodedMsg['data']['resubmit_status'] == 1) {
                ProcessList::where('process_type_id', 113)->where('ref_id', $ref_id)->update(
                    [
                        'status_id' => 17,  // force shortfall status when resubmit_status = 1, 17=Shortfall from CCIE
                        'desk_id' => 0
                    ]);

                CcieApplicationStatus::where('id', $id)
                    ->update(
                        [
                            'completed' => 1
                        ]);
            } elseif ($appStatus->status_id != $checkPrListStatus->status_id) {
                ProcessList::where('process_type_id', 113)->where('ref_id', $ref_id)->update(
                    [
                        'status_id' => $appStatus->status_id,
                        'desk_id' => 0
                    ]);
            }

        }

        CcieApplicationStatus::where('id', $id)
            ->update(
                [
                    'response' => $msg,
                    'ccie_status' => $ccie_status,
                    'status' => $status
                ]);
        if ($ccie_status == "Approved") {
            $curl = curl_init();
            $url = config('constant.ccie_api_url') . 'certificate-fetch';
            $appData = CcieApps::find($ref_id);

            $access_token = $this->ccieCommonFunction->getToken();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_CONNECTTIMEOUT => 15,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => '{
    "tracking_number": ' . $appData->ccie_tracking_no . '
}',
                CURLOPT_HTTPHEADER => array(
                    'Authorization: Bearer ' . $access_token,
                    'Content-Type: application/json',
                    "agent-id: " . config('constant.bida-agent-id')
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $result = json_decode($response, true);
            $certificate_url = $result['data']['certificate_url'];
            $appData->certificate = $certificate_url;
            $appData->save();
            CcieApplicationStatus::where('id', $id)
                ->update(
                    [
                        'completed' => 1
                    ]);
        }
    }


}
<?php


namespace App\Console\Commands\CCIE;

use App\Libraries\MonitorLog;
use App\Model\CCIE\CcieApps;
use App\Model\DOE\DoeFileRequest;
use App\Model\CCIE\CciePayConfirm;
use App\Model\DOE\DoeApiRequest;
use App\Model\DOE\DoeStatusMapping;
use App\ProcessList;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Process\Process;
use App\Model\CCIE\CcieChallanConfirm;


class CciePaymentConfirm extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccie:payconfirm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'after payment data sending ';

    /**
     * Create a new command instance.
     *
     */

    /*
     * File full path
     */
    private $file_path = '';
    private $ccieCommonFunction;
    public function __construct()
    {
        parent::__construct();
        $this->file_path = dirname(__FILE__)."/".basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
        $this->ccieCommonFunction = new CcieCommonFunction();
    }



    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle(){

        try{
            MonitorLog::cronAuditSave($this->file_path, 0, 0, 'pay-confirm[CCIE-4]');


            $request_datas = CciePayConfirm::orderBy('processing_at', 'asc')
                ->where('status', 0)
                ->limit(1)->get();

            if(count($request_datas) > 0){

                $access_token = $this->ccieCommonFunction->getToken();

                if($access_token != ''){

                    foreach ($request_datas as $req) {
                        $appData = CcieApps::find($req->ref_id);
                        if (!$appData->ccie_tracking_no) {
                            continue;
                        }

                        $url = config('constant.ccie_api_url') . 'store-challan';

                        $headers = array(
                            'Content-Type: application/json',
                            'Authorization: Bearer '.$access_token,
                        );

                        $reqdata = json_decode($req->request,true);
                        $postdata =$reqdata;
                        $postdata['application_id'] = $appData->ccie_tracking_no;
                        $req->ccie_request = json_encode($postdata);
                        $requestSend = Carbon::now()->format('Y-m-d H:i:s.u');
                        $response = $this->ccieCommonFunction->curlPostRequest($url,$headers, json_encode($postdata));
                        $responseget = Carbon::now()->format('Y-m-d H:i:s.u');$req->response = $response['data'];
                        $req->request_time = $requestSend;
                        $req->response_time = $responseget;
                        $req->processing_at = Carbon::now();
                        $req->no_of_try = $req->no_of_try + 1;

                        if($response['http_code'] == 200){

                            $message = $response['data'];

                            $response = json_decode($response['data']);
                            if(isset($response->responseCode) && $response->responseCode == '200'){

                                $req->status = 1;
                                $challanConfirm =CcieChallanConfirm::where('ref_id',$req->ref_id)->first();
                                $spgRequest = $challanConfirm->request_spg;
                                $lopt_url = $challanConfirm->spg_request_url;
                                $spgHeaders = array(
                                    "cache-control: no-cache",
                                    'Content-Type: application/json'
                                );
                                $spg_response = $this->ccieCommonFunction->curlPostRequest($lopt_url,$spgHeaders, $spgRequest);
                                if($spg_response['http_code'] == 200){
                                    $data2 =json_decode($spg_response['data']);
                                    $spgData =  json_decode($data2);
                                    $echallanData = [
                                        'reference_date' => $spgData->ReferenceDate,
                                        'challan_url' => $spgData->EchalUrl,
                                        'request_id' => $spgData->TrackingNo,
                                        'trn_amount' => ($spgData->TranAmount - 250)
                                    ];

                                    $postdata2['application_id'] = $appData->ccie_tracking_no;
                                    $postdata2['e_challan_payment_verify_data'] = $echallanData;
                                    $challanConfirm->request_ccie = json_encode($postdata2);
                                    $challanConfirm->status = 0;
                                    $challanConfirm->save();
                                }else{
                                    $challanConfirm->status = -1;
                                    $challanConfirm->save();
                                }

                            }else{
                                $req->status = -1;
                            }
                        }else{
                            $req->status = -3;
                        }

                        $req->save();

                    }
                }
            }else{
                echo "No data to submit";
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo $e->getLine();
            echo "Something went wrong";
        }
    }





}
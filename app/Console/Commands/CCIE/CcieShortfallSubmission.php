<?php


namespace App\Console\Commands\CCIE;

use App\Libraries\MonitorLog;
use App\Model\CCIE\CcieApplicationStatus;
use App\Model\CCIE\CCIEShortfall;
use Carbon\Carbon;
use Illuminate\Console\Command;


class CcieShortfallSubmission extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccie:shortfallSubmission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'shortfall data sending ';

    /**
     * Create a new command instance.
     *
     */

    /*
     * File full path
     */
    private $file_path = '';

    public function __construct()
    {
        parent::__construct();
        $this->file_path = dirname(__FILE__)."/".basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
        $this->ccieCommonFunction = new CcieCommonFunction();
    }



    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle(){

        try{
            MonitorLog::cronAuditSave($this->file_path, 0, 0, 'shortfall-submission[CCIE-4]');


            $request_datas = CCIEShortfall::orderBy('processing_at', 'asc')
                ->where('is_submit', 1)
                ->where('submission_status', 0)
                ->where('shortfall_submission_request','!=','')
                ->limit(1)->get();

            if(count($request_datas) > 0){
                $access_token = $this->ccieCommonFunction->getToken();
                if ($access_token != '') {
                    foreach ($request_datas as $req) {
                        $url = config('constant.ccie_api_url') . 'submit-shortfall';
                        $headers = array(
                            'Content-Type: application/json',
                            'Authorization: Bearer ' . $access_token,
                        );
                        $postdata = $req->shortfall_submission_request;
                        $requestSend = Carbon::now()->format('Y-m-d H:i:s.u');
                        $responses = $this->ccieCommonFunction->curlPostRequest($url, $headers, $postdata);
                        $responseget = Carbon::now()->format('Y-m-d H:i:s.u');
                        $req->request_time = $requestSend;
                        $req->response_time = $responseget;
                        $req->processing_at = Carbon::now();
                        $req->no_of_try = $req->no_of_try + 1;
                        $jsonResponseData = $responses['data'];

                        $req->submission_response = $jsonResponseData;

                        if ($responses['http_code'] == 200) {
                            $responseData = json_decode($jsonResponseData);
                            if (isset($responseData->responseCode) && $responseData->responseCode == '200') {
                                $status = 1;
                                CcieApplicationStatus::where('ref_id', $req->ref_id)
                                    ->update(
                                        [
                                            'completed' => 0
                                        ]);
                            } else {
                                $status = 0;
                            }
                        } else {
                            $status = -3;
                        }
                        $req->submission_status = $status;
                        $req->save();
                    }
                }
            }else{
                echo "No data to submit";
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo $e->getLine();
            echo "Something went wrong";
        }
    }





  
}
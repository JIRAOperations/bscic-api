<?php
/**
 * User: root
 * Date: 28/06/2020
 * Time: 3:47 PM
 */

namespace App\Console\Commands\CCIE;

use App\Libraries\MonitorLog;
use App\Model\CCIE\CCIEShortfall;
use App\Model\DOE\DoeMaster;
use App\Model\DOE\DoeFileRequest;
use App\Model\CDA\User;
use App\Model\CCIE\CciePaymentInfo;
use App\ProcessList;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Process\Process;


class CcieGetShortfallStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ccie:getshortfallstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get shortfall status';

    /**
     * Create a new command instance.
     *
     */

    /*
     * File full path
     */
    private $file_path = '';

    public function __construct()
    {
        parent::__construct();
        $this->file_path = dirname(__FILE__)."/".basename(__FILE__);
        $this->file_path = str_replace('\\', '/', $this->file_path);
        $this->ccieCommonFunction = new CcieCommonFunction();
    }


    /**
     * Execute the console command.
     * @return mixed
     */
    public function handle(){

        try{
            MonitorLog::cronAuditSave($this->file_path, 0, 0, 'ccie-get-shortfall-status[CCIE-3]');

            $request_datas = CCIEShortfall::orderBy('processing_at', 'asc')
                ->where('status',0)
                ->limit(1)->get();

            if(count($request_datas) > 0){
                $token = $this->ccieCommonFunction->getToken();
                foreach ($request_datas as $request_data) {
                    if ($token != '') {

                        $api_url = config('constant.ccie_api_url').'info/shortfall-status/'.$request_data->ccie_tracking_no;
                        // Get token for API authorization
                        $requestSend = Carbon::now()->format('Y-m-d H:i:s.u');
                        $response = $this->ccieCommonFunction->curlGetRequest($api_url,$token);
                        $responseget = Carbon::now()->format('Y-m-d H:i:s.u');

                        $request_data->response = $response['data'];
                        $request_data->request_time = $requestSend;
                        $request_data->response_time = $responseget;
                        $request_data->processing_at = Carbon::now();
                        $request_data->no_of_try = $request_data->no_of_try + 1;

                        if($response['http_code'] == 200){
                            $decodedResponse  =  json_decode($response['data']);
                            $comments = $decodedResponse->data->comment;
                            ProcessList::where('process_type_id',113)->where('ref_id',$request_data->ref_id)->update(['process_desc'=>$comments]);
                            $request_data->status = 1;
                        }
                        $request_data->save();
                    }
                }

            }else{
                echo "No data to submit";
            }
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo $e->getLine();
            echo "Something went wrong";
        }
    }


}